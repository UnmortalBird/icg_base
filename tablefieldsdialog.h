#ifndef TABLEFIELDSDIALOG_H
#define TABLEFIELDSDIALOG_H

#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QCheckBox>
#include <QPushButton>
#include <QSettings>
#include <QTextCodec>
#include <QDebug>


namespace Ui {
class TableFieldsDialog;
}

class TableFieldsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TableFieldsDialog(QStringList list, QWidget *parent = 0);
    ~TableFieldsDialog();

    QStringList fields();

private:
    Ui::TableFieldsDialog   *ui;
    QVBoxLayout             *vLayout;
    QVBoxLayout             *btnLayout;
    QHBoxLayout             *hLayout;
    QGridLayout             *gLayout;
    QPushButton             *btnOK;
    QPushButton             *btnCancel;
};

#endif // TABLEFIELDSDIALOG_H
