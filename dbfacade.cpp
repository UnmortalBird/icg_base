#include "dbfacade.h"

DBFacade::DBFacade(const QSqlDatabase &database, QObject *parent) :   QObject(parent)  {
    sortOrder = Qt::AscendingOrder;
    sortHeader = "";
    m_db = database;

    m_model = new QSqlQueryModel(this);

    QString str = "SELECT * FROM ICG.Strain "
                  "LEFT JOIN ICG.Colony ON ICG.Colony.idStrain_Colony = ICG.Strain.idStrain "
                  "LEFT JOIN ICG.BioChem ON ICG.BioChem.idStrain_BioChem = ICG.Strain.idStrain "
                  "LEFT JOIN ICG.Seq ON ICG.Seq.idStrain_Seq = ICG.Strain.idStrain "
                  "WHERE idStrain = ''";

    m_model->setQuery(str, m_db);

    m_second_1 = new BartProxyModel(this, m_db);
    m_second_1->setTable("AntiBio");
    m_second_1->setFilter("idStrain_AntiBio = '' ");
    m_second_1->select();
    m_second_1->setEditStrategy(QSqlTableModel::OnManualSubmit);

    m_second_2 = new BartProxyModel(this, m_db);
    m_second_2->setTable("Substrats");
    m_second_2->setFilter("idStrain_Substrats = '' ");
    m_second_2->select();
    m_second_2->setEditStrategy(QSqlTableModel::OnManualSubmit);

}


DBFacade::~DBFacade()   {
    delete m_model;
    delete m_second_1;
    delete m_second_2;
}



///////////////////////////////////////////////////////////////////////////////
void DBFacade::search(QString value, QString param)  {
    int idxCut = m_model->query().lastQuery().indexOf(" WHERE ");
    QString str = m_model->query().lastQuery().left(idxCut);

    if (value != "*")
        str += QString(" WHERE LOCATE ('%1', %2) ").arg(value).arg(param);
    else
        str += QString(" WHERE idStrain > 0 ");
    str += " ORDER BY idStrain ";
    m_model->setQuery(str, m_db);
}


QStringList DBFacade::searchID(QString value, QString param)  {
    QSqlTableModel *model = new QSqlTableModel(this, m_db);
    model->setTable("Strain");
    model->setFilter(QString("%1 = '%2'").arg(param, value));
    model->select();

    QStringList list;
    for (int i = 0; i < model->rowCount(); i++)    {
        list << model->record(i).value(0).toString();
    }
    delete model;

    return list;
}


void DBFacade::sort(QString header, Qt::SortOrder order)
{
    if (m_model->query().lastQuery().isEmpty()) return;

    if ((sortHeader != header) || (sortOrder != order))   {
        sortOrder = order;
        sortHeader = header;
    }

    int indexCut = m_model->query().lastQuery().indexOf(" ORDER BY");
    QString strCut = m_model->query().lastQuery().left(indexCut);

    QString str = QString("%1 ORDER BY `%2` %3")
            .arg(strCut)
            .arg(sortHeader)
            .arg((sortOrder == Qt::AscendingOrder) ? "ASC" : "DESC");

    m_model->setQuery(str, m_db);
}


void DBFacade::update()   {
    m_model->setQuery(m_model->query().lastQuery(), m_db);
}


///////////////////////////////////////////////////////////////////////////////
QVector<QStringList> DBFacade::rowValueVector(const QModelIndex &index) {
    QVector<QStringList> vecValues;
    int id = this->m_model->record(index.row()).value("idStrain").toInt();

    vecValues.push_back(rowValueList("ICG.Strain", "idStrain", id));
    vecValues.push_back(rowValueList("ICG.Colony", "idStrain_Colony", id));
    vecValues.push_back(rowValueList("ICG.BioChem", "idStrain_BioChem", id));
    vecValues.push_back(rowValueList("ICG.Seq", "idStrain_Seq", id));

    return vecValues;
}


QVector<QStringList> DBFacade::rowValueVector(const int id) {
    QVector<QStringList> vecValues;

    vecValues.push_back(rowValueList("ICG.Strain", "idStrain", id));
    vecValues.push_back(rowValueList("ICG.Colony", "idStrain_Colony", id));
    vecValues.push_back(rowValueList("ICG.BioChem", "idStrain_BioChem", id));
    vecValues.push_back(rowValueList("ICG.Seq", "idStrain_Seq", id));

    return vecValues;
}


QStringList DBFacade::rowValueList(QString tableName, QString header, int value)  {
    QSqlQuery q(QString("SELECT * FROM %1 WHERE %2 = %3")
                    .arg(tableName)
                    .arg(header)
                    .arg(value),
                m_db);

    QStringList list;
    if (q.next())   {
        for (int i = 0; i < q.record().count(); i++)
            list << q.value(i).toString();
    }

    return list;
}


int DBFacade::lastID(QString tableName) {
    QSqlQueryModel model;
    QString str = QString("SELECT * FROM %1 ORDER BY %1.id%1 ASC").arg(tableName);
    model.setQuery(str, m_db);

    return model.record(model.rowCount() - 1).value(0).toInt();
}



///////////////////////////////////////////////////////////////////////////////
void DBFacade::addItem(QString tableName, QStringList &values)
{
    QSqlTableModel *model = new QSqlTableModel(this, m_db);
    model->setTable(tableName);
    QSqlRecord record = model->record();
    record.remove(0);

    if ((values.size()) != record.count()) return;

    for (int i = 0; i < record.count(); i++)    {
        record.setValue(i, values[i]);
    }
    model->insertRecord(-1, record);
    delete model;
    update();
}


void DBFacade::editItem(QString tableName, int id, QStringList &values) {
    QSqlTableModel *model = new QSqlTableModel(this, m_db);
    model->setTable(tableName);
    model->select();

    if ((values.size() + 1) != model->columnCount()) return;

//    int id = QString("%1").arg(values[0]).toInt();

    int idx = 0;
    while (model->record(idx).value(0).toInt() != id)     { idx++;  }

    for (int i = 0; i < values.count(); i++)    {
        model->setData(model->index(idx, i+1), values[i]);
    }
    model->submitAll();
    delete model;
    update();
}


void DBFacade::delItem(QModelIndexList list)  {
    if (list.isEmpty())
        return;

    QList<int> listID;
    for (int i = 0; i < list.count(); i++)  {
        listID << m_model->record(list[i].row()).value("idStrain").toInt();
    }

    QSqlQuery q(m_db);
    foreach (int x, listID) {
        q.exec(QString("DELETE FROM ICG.Colony WHERE idStrain_Colony = %1")
               .arg(x));
        q.exec(QString("DELETE FROM ICG.BioChem WHERE idStrain_BioChem = %1")
               .arg(x));
        q.exec(QString("DELETE FROM ICG.Seq WHERE idStrain_Seq = %1")
               .arg(x));
        q.exec(QString("DELETE FROM ICG.AntiBio WHERE idStrain_AntiBio = %1")
               .arg(x));
        q.exec(QString("DELETE FROM ICG.Substrats WHERE idStrain_Substrats = %1")
               .arg(x));
        q.exec(QString("DELETE FROM ICG.Strain WHERE idStrain = %1")
               .arg(x));
    }
    update();
}



///////////////////////////////////////////////////////////////////////////////
void DBFacade::backupAll() {
    if (!QDir(pathStrain).exists())
        QDir().mkdir(pathStrain);

    backupTable(pathStrain + "/ICGBackup_Strain.csv",       "ICG.Strain");
    backupTable(pathStrain + "/ICGBackup_Colony.csv",       "ICG.Colony");
    backupTable(pathStrain + "/ICGBackup_BioChem.csv",      "ICG.BioChem");
    backupTable(pathStrain + "/ICGBackup_Seq.csv",          "ICG.Seq");
    backupTable(pathStrain + "/ICGBackup_AntiBio.csv",      "ICG.AntiBio");
    backupTable(pathStrain + "/ICGBackup_Substrats.csv",    "ICG.Substrats");
}


void DBFacade::backupTable(QString fileName, QString tableName) {
    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
    QTextCodec::setCodecForLocale(codec);

    QFile csvFile(fileName);

    QSqlQuery q(m_db);
    q.exec(QString("SELECT * FROM %1").arg(tableName));

    int columnCount = q.record().count();

    if(csvFile.open( QIODevice::WriteOnly ))    {
        QTextStream textStream( &csvFile );
        QStringList stringList;

        while(q.next())   {
            stringList.clear();
            for( int column = 0; column < columnCount; column++ ) {
                stringList << q.value(column).toString();
            }
            textStream << stringList.join( ';' )+"\n";
        }
        csvFile.close();
    }
}


void DBFacade::restoreAll() {
    if (!QDir(pathStrain).exists()) {
        QMessageBox::information(0, tr("Импорт данных"), tr("Резервная копия не найдена. Указан неверный адрес."));
        return;
    }

    eraseTable("ICG.Colony");
    eraseTable("ICG.Strain");
    eraseTable("ICG.BioChem");
    eraseTable("ICG.Seq");
    eraseTable("ICG.AntiBio");
    eraseTable("ICG.Substrats");
    restoreTable(pathStrain + "/ICGBackup_Strain.csv",      "ICG.Strain");
    restoreTable(pathStrain + "/ICGBackup_Colony.csv",      "ICG.Colony");
    restoreTable(pathStrain + "/ICGBackup_BioChem.csv",     "ICG.BioChem");
    restoreTable(pathStrain + "/ICGBackup_Seq.csv",         "ICG.Seq");
    restoreTable(pathStrain + "/ICGBackup_AntiBio.csv",     "ICG.AntiBio");
    restoreTable(pathStrain + "/ICGBackup_Substrats.csv",   "ICG.Substrats");

    update();
}


void DBFacade::restoreTable(QString fileName, QString tableName) {

    QSqlQuery q("SET character_set_database = cp1251", m_db);

    q.exec(QString("LOAD DATA LOCAL INFILE '%1' INTO TABLE %2"
                   " FIELDS TERMINATED BY ';' "
                   " LINES TERMINATED BY '\n' ")
            .arg(fileName)
            .arg(tableName)
           );
}


void DBFacade::eraseTable(QString tableName)  {
    QSqlQuery q(QString("DELETE FROM %1").arg(tableName), m_db);
}


///////////////////////////////////////////////////////////////////////////////
void DBFacade::importBaseFromFile()   {
    QString fileName = QFileDialog::getOpenFileName(0, tr("Импорт данных"), QCoreApplication::applicationDirPath(), tr ("Microsoft Excel (*.xls)"));
    if (!fileName.isEmpty())    {
        BartProxyModel *model = new BartProxyModel(this, m_db);
        model->setTable("Strain");
        model->select();

        QAxObject * excel = new QAxObject("Excel.Application",this);
        QAxObject * workbooks = excel->querySubObject("Workbooks");
        QAxObject * workbook = workbooks->querySubObject("Open(const QString&)", fileName);
        QAxObject * sheets = workbook->querySubObject("Sheets");
        QAxObject * StatSheet = sheets->querySubObject("Item(1)");
        StatSheet->dynamicCall("Select()");

        int cntNewRow = 0;
        QSqlRecord record = model->record();
        record.remove(0);
        for (int row = 2; !StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").isNull(); row++)   {
            int dupRow = 0;
            bool duplicate = false;
            while (dupRow < model->rowCount() && !duplicate)  {
                if ( (model->record(dupRow).value("Name") == StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").toString()) &&
                     (model->record(dupRow).value("ExtrEnv") == StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 5)->property("Value").toString())  &&
                     (model->record(dupRow).value("Temp") == StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 6)->property("Value").toString()) )
                    duplicate = true;
                dupRow++;
            }
            if (duplicate)   {
                QMessageBox::information(0, tr("Импорт данных"), tr("Такие данные уже присутствуют в базе данных: строка №") + QString::number(row));
            } else {
                for (int col = 1; col < model->columnCount(); col++)   {
                    QAxObject * cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", row, col);
                    record.setValue(col-1, cell->property("Value").toString());
                    delete cell;
                }
                cntNewRow++;
                model->insertRecord(-1, record);
            }
        }
        // memory clearing
        delete StatSheet;
        delete sheets;
        workbook->dynamicCall("Close (Boolean)", false);
        delete workbook;
        delete workbooks;
        excel->dynamicCall("Quit(void)");
        delete excel;
        delete model;

        this->update();
        QMessageBox::information(0, tr("Импорт данных"), tr("Строк добавлено: ") + QString::number(cntNewRow));
    }
}


void DBFacade::updateBaseFromFile()   {
    QString fileName = QFileDialog::getOpenFileName(0, tr("Обновление данных"), QCoreApplication::applicationDirPath(), tr ("Microsoft Excel (*.xls)"));
    if (!fileName.isEmpty())    {
        BartProxyModel *model = new BartProxyModel(this, m_db);
        model->setTable("Strain");
        model->select();

        QAxObject * excel = new QAxObject("Excel.Application",this);
        QAxObject * workbooks = excel->querySubObject("Workbooks");
        QAxObject * workbook = workbooks->querySubObject("Open(const QString&)", fileName);
        QAxObject * sheets = workbook->querySubObject("Sheets");
        QAxObject * StatSheet = sheets->querySubObject("Item(1)");
        StatSheet->dynamicCall("Select()");

        int cntUpdatedRow = 0;
        for (int row = 2; !StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").isNull(); row++)   {
            int dupRow = 0;
            bool duplicate = false;

            while (dupRow < model->rowCount() && !duplicate)  {
                if ( (model->record(dupRow).value("Name") == StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").toString()) &&
                     (model->record(dupRow).value("ExtrEnv") == StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 5)->property("Value").toString())  &&
                     (model->record(dupRow).value("Temp") == StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 6)->property("Value").toString()) )
                    duplicate = true;
                else dupRow++;
            }

            if (!duplicate)   {
                QMessageBox::information(0, tr("Обновление данных"), tr("Такие данные отсутствуют в базе данных: строка №") + QString::number(row));
            } else {
                for (int col = 1; col < model->columnCount(); col++)   {
                    QAxObject * cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", row, col);
                    model->setData(model->index(dupRow, col), cell->property("Value").toString());
                    model->submit();
                    delete cell;
                }
                cntUpdatedRow++;
            }
        }
        // memory clearing
        delete StatSheet;
        delete sheets;
        workbook->dynamicCall("Close (Boolean)", false);
        delete workbook;
        delete workbooks;
        excel->dynamicCall("Quit(void)");
        delete excel;
        delete model;

        this->update();
        QMessageBox::information(0, tr("Обновление данных"), tr("Строк изменено: ") + QString::number(cntUpdatedRow));
    }
}



void DBFacade::importNotBaseFromFile(QString tableName, QString uniqueField)    {
    QString fileName = QFileDialog::getOpenFileName(0, tr("Импорт данных"), QCoreApplication::applicationDirPath(), tr ("Microsoft Excel (*.xls)"));
    if (!fileName.isEmpty())    {
        BartProxyModel *model = new BartProxyModel(this, m_db);
        model->setTable(tableName);
        model->select();

        QAxObject * excel = new QAxObject("Excel.Application",this);
        QAxObject * workbooks = excel->querySubObject("Workbooks");
        QAxObject * workbook = workbooks->querySubObject("Open(const QString&)", fileName);
        QAxObject * sheets = workbook->querySubObject("Sheets");
        QAxObject * StatSheet = sheets->querySubObject("Item(1)");
        StatSheet->dynamicCall("Select()");

        QSqlRecord record = model->record();
        int intField = record.indexOf(uniqueField);
        int cntNewRow = 0;
        record.remove(0);
        for (int row = 2; !StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").isNull(); row++)   {
            int dupRow = 0;
            bool duplicate = false;
            while (dupRow < model->rowCount() && !duplicate)  {
                if ( (model->record(dupRow).value(uniqueField) == StatSheet->querySubObject("Cells(QVariant,QVariant)", row, intField+1)->property("Value").toString()) )
                    duplicate = true;
                dupRow++;
            }
            if (duplicate)   {
                QMessageBox::information(0, tr("Импорт данных"), tr("Такие данные уже присутствуют в базе данных: строка №") + QString::number(row));
            } else {
                for (int col = 1; col < model->columnCount(); col++)   {
                    QAxObject * cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", row, col);
                    record.setValue(col-1, cell->property("Value").toString());
                    delete cell;
                }
                cntNewRow++;
                model->insertRecord(-1, record);
            }
        }
        // memory clearing
        delete StatSheet;
        delete sheets;
        workbook->dynamicCall("Close (Boolean)", false);
        delete workbook;
        delete workbooks;
        excel->dynamicCall("Quit(void)");
        delete excel;
        delete model;

        this->update();
        QMessageBox::information(0, tr("Импорт данных"), tr("Строк добавлено: ") + QString::number(cntNewRow));
    }
}


void DBFacade::updateNotBaseFromFile(QString tableName, QString uniqueField)    {
    QString fileName = QFileDialog::getOpenFileName(0, tr("Обновление данных"), QCoreApplication::applicationDirPath(), tr ("Microsoft Excel (*.xls)"));
    if (!fileName.isEmpty())    {
        BartProxyModel *model = new BartProxyModel(this, m_db);
        model->setTable(tableName);
        model->select();

        QAxObject * excel = new QAxObject("Excel.Application",this);
        QAxObject * workbooks = excel->querySubObject("Workbooks");
        QAxObject * workbook = workbooks->querySubObject("Open(const QString&)", fileName);
        QAxObject * sheets = workbook->querySubObject("Sheets");
        QAxObject * StatSheet = sheets->querySubObject("Item(1)");
        StatSheet->dynamicCall("Select()");

        int cntUpdatedRow = 0;
        int intField = model->record().indexOf(uniqueField);
        qDebug() << model->record().indexOf(uniqueField);
        for (int row = 2; !StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").isNull(); row++)   {
            int dupRow = 0;
            bool duplicate = false;

            while (dupRow < model->rowCount() && !duplicate)  {
                if ( (model->record(dupRow).value(uniqueField) == StatSheet->querySubObject("Cells(QVariant,QVariant)", row, intField)->property("Value").toString()) )
                    duplicate = true;
                else dupRow++;
            }

            if (!duplicate)   {
                QMessageBox::information(0, tr("Обновление данных"), tr("Такие данные отсутствуют в базе данных: строка №") + QString::number(row));
            } else {
                for (int col = 1; col < model->columnCount(); col++)   {
                    QAxObject * cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", row, col);
                    model->setData(model->index(dupRow, col), cell->property("Value").toString());
                    model->submit();
                    delete cell;
                }
                cntUpdatedRow++;
            }
        }
        // memory clearing
        delete StatSheet;
        delete sheets;
        workbook->dynamicCall("Close (Boolean)", false);
        delete workbook;
        delete workbooks;
        excel->dynamicCall("Quit(void)");
        delete excel;
        delete model;

        this->update();
        QMessageBox::information(0, tr("Обновление данных"), tr("Строк изменено: ") + QString::number(cntUpdatedRow));
    }
}



///////////////////////////////////////////////////////////////////////////////
BartProxyModel * DBFacade::getSecondaryModel(QString tableName, int id)    {
    BartProxyModel *tableModel = new BartProxyModel(this, m_db);
    tableModel->setTable(tableName);

    tableModel->setFilter(QString("idStrain_%1 = %2").arg(tableName, QString::number(id)));
    tableModel->select();

    return tableModel;

//    if (m_second_1->tableName() == tableName)
//        return m_second_1;
//    if (m_second_2->tableName() == tableName)
//        return m_second_2;
//    return 0;
}



///////////////////////////////////////////////////////////////////////////////
//void DBFacade::addFake()    {
//    int maxStrain = 1000;
//    int maxAntiBio = 30000;
//    int maxSubstrats = 5000;
//    QString str;

//    QSqlTableModel * m_main = new QSqlTableModel(this, m_db);
//    m_main->setTable("Strain");
//    m_main->select();
//    for (int i = 6; i < maxStrain; i++)   {
//        qDebug() << "Strain#" << i;
//        int lastRow = m_main->rowCount();
//        m_main->insertRow(lastRow);
//        m_main->setData(m_main->index(lastRow, 0), QString::number(i).rightJustified(4, '0'));
//        m_main->setData(m_main->index(lastRow, 1), QString("Strain_%1").arg(i));
//        m_main->setData(m_main->index(lastRow, 2), QString("Place_%1").arg(qrand() % 20 + 1));
//        m_main->setData(m_main->index(lastRow, 4), QDate::fromString("01.02.1989", "dd.MM.yyyy"));
//        m_main->setData(m_main->index(lastRow, 7), "Tax");
//        m_main->submitAll();
//    }

//    m_main->setTable("AntiBio");
//    m_main->select();
//    for (int i = 30; i < maxAntiBio; i++)   {
//        qDebug() << "AntiBio#" << i;
//        int lastRow = m_main->rowCount();
//        m_main->insertRow(lastRow);
//        m_main->setData(m_main->index(lastRow, 0), i);
//        m_main->setData(m_main->index(lastRow, 1), QString("Antibiotic_%1").arg(qrand() % 100 + 1));
//        m_main->setData(m_main->index(lastRow, 2), "+- ");
//        str = QString::number(qrand() % (maxStrain-1) + 1).rightJustified(4, '0');
//        m_main->setData(m_main->index(lastRow, 4), str);
//        qDebug() << str;
//        m_main->submitAll();
//    }

//    m_main->setTable("Substrats");
//    m_main->select();
//    for (int i = 1; i < maxSubstrats; i++)   {
//        qDebug() << "Substrat#" << i;
//        int lastRow = m_main->rowCount();
//        m_main->insertRow(lastRow);
//        m_main->setData(m_main->index(lastRow, 0), i);
//        m_main->setData(m_main->index(lastRow, 1), QString("Substrat_%1").arg(qrand() % 10 + 1));
//        m_main->setData(m_main->index(lastRow, 2), "+- ");
//        str = QString::number(qrand() % (maxStrain-1) + 1).rightJustified(4, '0');
//        m_main->setData(m_main->index(lastRow, 4), str);
//        qDebug() << str;
//        m_main->submitAll();
//    }

//    delete m_main;
//}

//void DBFacade::delFake()    {
//    QSqlTableModel * m_second_1 = new QSqlTableModel(this, m_db);
//    m_second_1->setTable("AntiBio");
//    m_second_1->select();
//    for (int i = 0; i < m_second_1->rowCount(); i++)   {
//        if (m_second_1->data(m_second_1->index(i, 1)).toString().contains("Antibiotic_"))   {
//            m_second_1->removeRow(i);
//            qDebug() << "AntiBio#" << i;
//        }
//    }

//    m_second_1->setTable("Substrats");
//    m_second_1->select();
//    for (int i = 0; i < m_second_1->rowCount(); i++)   {
//        if (m_second_1->data(m_second_1->index(i, 1)).toString().contains("Substrat_"))   {
//            m_second_1->removeRow(i);
//            qDebug() << "Substrat#" << i;
//        }
//    }
//    delete m_second_1;


//    QSqlTableModel * m_main = new QSqlTableModel(this, m_db);
//    m_main->setTable("Strain");
//    m_main->select();
//    for (int i = 0; i < m_main->rowCount(); i++)   {
//        if (m_main->data(m_main->index(i, 1)).toString().contains("Strain_"))   {
//            m_main->removeRow(i);
//            qDebug() << "Strain#" << i;
//        }
//    }
//    delete m_main;
//}



///////////////////////////////////////////////////////////////////////////////

//int DBFacade::newID(int tableIdx)   {
//    QString tableName;
//    switch(tableIdx)   {
//        case 1  :   {
//            tableName = "ICG.Colony";
//            break;
//        }
//        case 2  :   {
//            tableName = "ICG.BioChem";
//            break;
//        }
//        default :   {
//            tableName = "ICG.Strain";
//            break;
//        }
//    }
////    QSqlQuery q("SELECT MAX(idStrain) FROM ICG.Strain", m_db);
////    q.next();
//    QSqlQuery q(QString("SELECT * FROM %1").arg(tableName), m_db);

////  q.seek(q.numRowsAffected()-1);

//    return q.size() + 1;
//}


//void DBFacade::editAll(QString tableName, QStringList &headersAll, QStringList &values)  {
//    int offset = 0;
//    QString tableName = "";
//    qDebug() << tableIdx;
//    switch(tableName)   {
//        case 1  :   {
//            offset = 12;
//            tableName = "ICG.Colony";
//            break;
//        }
//        case 2  :   {
//            offset = 28;
//            tableName = "ICG.BioChem";
//            break;
//        }
//        default :   {
//            offset = 0;
//            tableName = "ICG.Strain";
//            break;
//        }
//    }

//    QStringList headersActual;
//    for (int i = 0; i < values.count(); i++)  {
//        headersActual << headersAll[i+offset];
//    }
////    qDebug() << tableName;
////    qDebug() << headersActual;
////    qDebug() << values << endl;
//    editItem(tableName, headersActual, values);
//}


//void DBFacade::editItem(QString tableName, QStringList &headers, QStringList &values) {
//    QSqlQuery q(QString("SELECT * FROM ICG.%1 WHERE %2 = %3")
//                    .arg(tableName)
//                    .arg(headers[0])
//                    .arg(values[0]),
//                m_db);
////    qDebug() << "value.size = " << values.size()
////             << "     header.size = " << headers.size()
////             << "     rec.count = " << q.record().count();
//    if (  (values.size() != headers.size()) || (values.size() != q.record().count())  ) return;

//    QString str = QString("UPDATE ICG.%1 SET ").arg(tableName);
//    for (int i = 1; i < headers.size(); i++) {
//        if (QString(headers[i]).contains(' '))
//            str += QString(" '%1' = '%2',").arg(headers[i]).arg(values[i]);
//        else
//            str += QString(" %1 = '%2',").arg(headers[i]).arg(values[i]);
//    }
//    str.chop(1);
//    str += QString(" WHERE %1 = %2").arg(headers[0]).arg(values[0]);

////    qDebug() << str ;
////    qDebug() << "================================================" << endl;
//    q.exec(str);
//    update();
//}


//void DBFacade::setFields(QList<QPair<QString, QString>> list)  {
//    int indexCut = m_model->query().lastQuery().indexOf(" FROM");
//    QString strCut = m_model->query().lastQuery().mid(indexCut);

//    QString str = "SELECT ";

//    for (int i = 0; i < list.count(); i++)
//        str += QString("`%1` as '%2', ").arg(list[i].first).arg(list[i].second);
//    str.chop(2);

//    str += strCut;

//    m_model->setQuery(str, m_db);
////    qDebug() << str;
////    qDebug() << m_model->query().lastQuery();
//}
