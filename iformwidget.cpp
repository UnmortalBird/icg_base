#include "iformwidget.h"

IFormWidget::IFormWidget(QObject *parent)
    : QObject(parent)
{
    _editable = false;
}


IFormWidget::IFormWidget(QWidget *view, QString type, QObject *parent)
    : QObject(parent)
{
    wView = view;
    _typeControl = type;
    _editable = false;
}



IFormWidget::IFormWidget(QWidget *view, QWidget *control, QObject *parent)
    : QObject(parent)
{
    wView = view;
    wControl = control;
    _editable = false;
}


IFormWidget::~IFormWidget()
{}



///////////////////////////////////////////////////////////////////////////////
void IFormWidget::setViewValue(QString str)  {
    if (qobject_cast<QLineEdit *>(this->wView))  {
        qobject_cast<QLineEdit *>(this->wView)->setText(str);
    }
    else if (qobject_cast<QTextEdit *>(this->wView)) {
        qobject_cast<QTextEdit *>(this->wView)->setPlainText(str);
        qobject_cast<QTextEdit *>(this->wView)->setAlignment(Qt::AlignRight);
    }
    else if (qobject_cast<QDateEdit *>(this->wView))  {
        qobject_cast<QDateEdit *>(this->wView)->setDate(QDate::fromString(str, "dd.MM.yyyy"));
    }
}


void IFormWidget::setValidator(QString strRegExp)  {
    if ((qobject_cast<QLineEdit *>(this->wView)) && (!strRegExp.isEmpty()))  {
        QRegExp rx(strRegExp);
        qobject_cast<QLineEdit *>(this->wView)->setValidator(new QRegExpValidator(rx, this));
    }
}


void IFormWidget::setEditable(bool edit, int row)  {
    if (this->typeControl() == "")  {
        if (qobject_cast<QLineEdit *>(this->wView))  {
            qobject_cast<QLineEdit *>(this->wView)->setReadOnly(!edit);
        }
        else if (qobject_cast<QTextEdit *>(this->wView)) {
            qobject_cast<QTextEdit *>(this->wView)->setReadOnly(!edit);
            if(!edit)   {
                QString str = qobject_cast<QTextEdit *>(this->wView)->toPlainText();
                str.replace("\n", " ");
                str.replace("\t", " ");
                qobject_cast<QTextEdit *>(this->wView)->setPlainText(str);
            }
            qobject_cast<QTextEdit *>(this->wView)->setAlignment(Qt::AlignRight);
        }
        else if (qobject_cast<QDateEdit *>(this->wView))  {
            qobject_cast<QDateEdit *>(this->wView)->setReadOnly(!edit);
        }
    }
    else if(this->typeControl() == "CheckBox")   {
        if (!edit && !this->_editable)
            qobject_cast<QLineEdit *>(this->wView)->setReadOnly(1);
        else if (edit && !this->_editable)   {
            this->LineEditToCheckBox(row);
            _editable = true;
        }
        else if (!edit && this->_editable)  {
            this->CheckBoxToLineEdit(row);
            _editable = false;
        }      
    }
    else if (this->typeControl() == "ComboBox")   {
        if (!edit && !this->_editable)
            qobject_cast<QLineEdit *>(this->wView)->setReadOnly(1);
        else if (edit && !this->_editable)   {
            this->LineEditToComboBox(row);
            _editable = true;
        }
        else if (!edit && this->_editable)  {
            this->ComboBoxToLineEdit(row);
            _editable = false;
        }
    }
    else if (this->typeControl() == "DoubleComboBox")   {
        if (!edit && !this->_editable)
            qobject_cast<QLineEdit *>(this->wView)->setReadOnly(1);
        else if (edit && !this->_editable)   {
            this->LineEditToDoubleComboBox(row);
            _editable = true;
        }
        else if (!edit && this->_editable)  {
            this->DoubleComboBoxToLineEdit(row);
            _editable = false;
        }
    }
}


QString IFormWidget::getWidgetData()    {
    if (qobject_cast<QLineEdit *>(this->wView))  {
        return qobject_cast<QLineEdit *>(this->wView)->text();
    }
    if (qobject_cast<QTextEdit *>(this->wView)) {
        return qobject_cast<QTextEdit *>(this->wView)->toPlainText();
    }
    if (qobject_cast<QDateEdit *>(this->wView))  {

        return qobject_cast<QDateEdit *>(this->wView)->text();
    }
    return "";
}



///////////////////////////////////////////////////////////////////////////////
void IFormWidget::LineEditToCheckBox(int row)   {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(this->viewWidget()->parent())->layout());
    QStringList list = static_cast<QLineEdit *>(this->viewWidget())->text().split(" / ");
    QGridLayout *layoutClass = new QGridLayout;
    QSettings settings("settings.ini", QSettings::IniFormat);

    static_cast<QLineEdit*>(this->viewWidget())->hide();
    grid->removeWidget(this->viewWidget());

    for (int i = 0; settings.value(QString("%1/Item_%2").arg(this->associatedParam()).arg(i)).isValid(); i++)  {
        QCheckBox *checkBox = new QCheckBox(settings.value(QString("%1/Item_%2").arg(this->associatedParam()).arg(i)).toString());
        layoutClass->addWidget(checkBox, i/2, i%2);
        if (list.contains(checkBox->text()))
            checkBox->setChecked(true);
    }
    grid->addLayout(layoutClass, row, 1);
}


void IFormWidget::CheckBoxToLineEdit(int row)   {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(this->viewWidget()->parent())->layout());
    QGridLayout *layoutClass = static_cast<QGridLayout *>(grid->itemAtPosition(row, 1)->layout());
    QStringList listClass;

    while (layoutClass->count())  {
        QCheckBox *checkBox = static_cast<QCheckBox*>(layoutClass->itemAt(0)->widget());
        if (checkBox->isChecked())
            listClass << checkBox->text();
        layoutClass->removeWidget(checkBox);
        delete checkBox;
    }

    delete layoutClass;
    grid->addWidget(this->viewWidget(), row, 1);
    this->viewWidget()->show();

    static_cast<QLineEdit *>(this->viewWidget())->setText(listClass.join(" / "));
}


void IFormWidget::LineEditToComboBox(int row)   {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(this->viewWidget()->parent())->layout());
    QSettings settings("settings.ini", QSettings::IniFormat);

    static_cast<QLineEdit*>(this->viewWidget())->hide();
    grid->removeWidget(this->viewWidget());

    QComboBox *comboBox = new QComboBox();
    for (int i = 0; settings.value(QString("%1/Item_%2").arg(this->associatedParam()).arg(i)).isValid(); i++)
        comboBox->addItem(settings.value(QString("%1/Item_%2").arg(this->associatedParam()).arg(i)).toString());

    grid->addWidget(comboBox, row, 1);
}


void IFormWidget::ComboBoxToLineEdit(int row)   {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(this->viewWidget()->parent())->layout());
    QComboBox *comboBox = static_cast<QComboBox *>(grid->itemAtPosition(row, 1)->widget());
    QString value = comboBox->currentText();

    grid->removeWidget(comboBox);
    delete comboBox;

    grid->addWidget(this->viewWidget(), row, 1);
    this->viewWidget()->show();

    static_cast<QLineEdit *>(this->viewWidget())->setText(value);
}


void IFormWidget::LineEditToDoubleComboBox(int row)   {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(this->viewWidget()->parent())->layout());
    QSettings settings("settings.ini", QSettings::IniFormat);

    static_cast<QLineEdit*>(this->viewWidget())->hide();
    grid->removeWidget(this->viewWidget());

    QComboBox *comboBox_1 = new QComboBox();
    for (int i = 0; settings.value(QString("%1/Item_%2").arg(this->associatedParam()).arg(i)).isValid(); i++)
        comboBox_1->addItem(settings.value(QString("%1/Item_%2").arg(this->associatedParam()).arg(i)).toString());

    QComboBox *comboBox_2 = new QComboBox();
    for (int i = 0; settings.value(QString("%1/Item_%2").arg(this->associatedParam()).arg(i)).isValid(); i++)
        comboBox_2->addItem(settings.value(QString("%1/Item_%2").arg(this->associatedParam()).arg(i)).toString());

    QHBoxLayout *comboLayout = new QHBoxLayout;
    comboLayout->addWidget(comboBox_1);
    comboLayout->addWidget(comboBox_2);
    grid->addLayout(comboLayout,row, 1);
}


void IFormWidget::DoubleComboBoxToLineEdit(int row) {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(this->viewWidget()->parent())->layout());
    QHBoxLayout *comboLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(row, 1)->layout());
    QComboBox *comboBox_1 = static_cast<QComboBox *>(comboLayout->itemAt(0)->widget());
    QComboBox *comboBox_2 = static_cast<QComboBox *>(comboLayout->itemAt(1)->widget());
    QString value = comboBox_1->currentText() + " / " + comboBox_2->currentText();

    comboLayout->removeWidget(comboBox_1);
    delete comboBox_1;
    comboLayout->removeWidget(comboBox_2);
    delete comboBox_2;
    delete comboLayout;

    grid->addWidget(this->viewWidget(), row, 1);
    this->viewWidget()->show();

    static_cast<QLineEdit *>(this->viewWidget())->setText(value);
}


