#include "valueslistdialog.h"
#include "ui_valueslistdialog.h"


ValuesListDialog::ValuesListDialog(QString groupName, QString itemName, QString dialogName, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ValuesListDialog)
{
    ui->setupUi(this);
    this->setWindowTitle(dialogName);

    QSettings settings("settings.ini", QSettings::IniFormat);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    settings.setIniCodec(codec);

    QStringList list;

    for (int i = 0; settings.value(QString("%1/%2_%3").arg(groupName).arg(itemName).arg(i)).isValid(); i++)
        list << settings.value(QString("%1/%2_%3").arg(groupName).arg(itemName).arg(i)).toString();

    model = new QStringListModel;
    model->setStringList(list);
    ui->listView->setModel(model);

    connect(ui->btnOK,          &QPushButton::clicked,          this,           &ValuesListDialog::btnOK_clicked);
    connect(ui->btnAbort,       &QPushButton::clicked,          this,           &ValuesListDialog::btnAbort_clicked);
    connect(ui->btnAdd,         &QPushButton::clicked,          this,           &ValuesListDialog::btnAdd_clicked);
    connect(ui->btnEdit,        &QPushButton::clicked,          this,           &ValuesListDialog::btnEdit_clicked);
    connect(ui->btnDel,         &QPushButton::clicked,          this,           &ValuesListDialog::btnDel_clicked);
}

ValuesListDialog::~ValuesListDialog()
{
    delete ui;
    delete model;
}

void ValuesListDialog::btnOK_clicked()
{
    accept();
}

void ValuesListDialog::btnAbort_clicked()
{
    reject();
}

void ValuesListDialog::btnAdd_clicked()
{
    model->insertRow(model->rowCount());
    model->setData(model->index(model->rowCount()-1), "Новое значение");
    ui->listView->edit(model->index(model->rowCount()-1));
}

void ValuesListDialog::btnEdit_clicked()
{
    ui->listView->edit(ui->listView->currentIndex());
}

void ValuesListDialog::btnDel_clicked()
{
    model->removeRow(ui->listView->currentIndex().row());
}

QStringList ValuesListDialog::value_list()
{
    QStringList list;
    for (int i = 0; i < model->rowCount(); i++)
        list << model->data(model->index(i)).toString();

    return list;
}
