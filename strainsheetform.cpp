#include "strainsheetform.h"
#include "ui_strainsheetform.h"


StrainSheetForm::StrainSheetForm(QVector<QStringList> &vecValues, BartProxyModel *second_1, BartProxyModel *second_2, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StrainSheetForm)
{
    ui->setupUi(this);
    this->setWindowTitle("Карта штамма");

    wBase = new IFormWidget[static_cast<QGridLayout *>(ui->boxBase->layout())->rowCount()-1];
    setBaseStringList();
    if (!vecValues[0].isEmpty())    {
        id = static_cast<QString>(vecValues[0][0]);
        setBoxData(ui->boxBase, wBase, vecValues[0]);
        setViewButtons(ui->boxBase, wBase, false);
    } else {
        id = "0";
        setViewButtons(ui->boxBase, wBase, true);
        fillEmptyBox(ui->boxBase, wBase);
    }
    for (int i = 0; i < static_cast<QGridLayout *>(ui->boxBase->layout())->rowCount()-1; i++)
        wBase[i].setEditable(false, i);

    wColony = new IFormWidget[static_cast<QGridLayout *>(ui->boxColony->layout())->rowCount()-1];
    setColonyStringList();
    if (!vecValues[1].isEmpty())    {
        setBoxData(ui->boxColony, wColony, vecValues[1]);
        setViewButtons(ui->boxColony, wColony, false);
    } else {
        setViewButtons(ui->boxColony, wColony, true);
        fillEmptyBox(ui->boxColony, wColony);
    }
    for (int i = 0; i < static_cast<QGridLayout *>(ui->boxColony->layout())->rowCount()-1; i++)
        wColony[i].setEditable(false, i);

    wBioChem = new IFormWidget[static_cast<QGridLayout *>(ui->boxBioChem->layout())->rowCount()-1];
    setBioChemStringList();
    if (!vecValues[2].isEmpty())    {
        setBoxData(ui->boxBioChem, wBioChem, vecValues[2]);
        setViewButtons(ui->boxBioChem, wBioChem, false);
    } else {
        setViewButtons(ui->boxBioChem, wBioChem, true);
        fillEmptyBox(ui->boxBioChem, wBioChem);
    }
    for (int i = 0; i < static_cast<QGridLayout *>(ui->boxBioChem->layout())->rowCount()-1; i++)
        wBioChem[i].setEditable(false, i);


    if (!vecValues[3].isEmpty())    {
        setBoxData(ui->boxSeq, vecValues[3]);
        setViewButtons(ui->boxSeq, false);
    } else {
        setViewButtons(ui->boxSeq, true);
        listSeq << "" << "" << "" << "" << "" << "" << "";
    }

    mAntiBio = second_1;
    mSubstrats = second_2;
    setViewButtonsDuo(ui->boxAntiBio);
    setViewButtonsDuo(ui->boxSubstrats);
    if (id.toInt()) {
        setBoxDataDuo(ui->boxAntiBio, mAntiBio);
        setBoxDataDuo(ui->boxSubstrats, mSubstrats);
    }

    this->setAttribute(Qt::WA_DeleteOnClose);
}


StrainSheetForm::~StrainSheetForm()
{
    delete ui;
    delete [] wBase;
    delete [] wColony;
    delete [] wBioChem;
    delete mAntiBio;
    delete mSubstrats;
}



///////////////////////////////////////////////////////////////////////////////
void StrainSheetForm::setBaseStringList() {
    QGridLayout *grid = static_cast<QGridLayout *>(ui->boxBase->layout());

    for (int i = 0; i < grid->rowCount()-1; i++)    {
        wBase[i].setID("Strain");
        wBase[i].setViewWidget(grid->itemAtPosition(i, 1)->widget());
        wBase[i].setValidator("");
        wBase[i].setNotEmpty(false);
        wBase[i].setTypeControl("");
        wBase[i].setAssociatedParam("");
    }
//    wBase[3].setValidator("[^\t\n\f]");
    wBase[6].setValidator("^[\\-]?\\d{0,2}([\\.|,]\\d{0,2})$");
    wBase[7].setValidator("[a-zA-Z\\s]*");
    wBase[8].setValidator("[a-zA-Z\\d]*");
    wBase[10].setValidator("\\d\\d?\\d?");
    wBase[11].setValidator("\\d\\d?\\d?");

    wBase[1].setNotEmpty(true);
    wBase[2].setNotEmpty(true);
    wBase[4].setNotEmpty(true);
    wBase[5].setNotEmpty(true);
    wBase[6].setNotEmpty(true);

    wBase[9].setTypeControl("CheckBox");
    wBase[9].setAssociatedParam("Strain_Classes");
}


void StrainSheetForm::setColonyStringList()    {
    QGridLayout *grid = static_cast<QGridLayout *>(ui->boxColony->layout());

    for (int i = 0; i < grid->rowCount()-1; i++)    {
        wColony[i].setID("Colony");
        wColony[i].setViewWidget(grid->itemAtPosition(i, 1)->widget());
        wColony[i].setValidator("");
        wColony[i].setNotEmpty(false);
        wColony[i].setTypeControl("");
        wColony[i].setAssociatedParam("");
    }
    wColony[3].setValidator("^\\d{0,2}([\\.|,]\\d{0,2})$");
    wColony[4].setValidator("^\\d{0,2}([\\.|,]\\d{0,2})$");
    wColony[5].setValidator("^\\d{0,2}([\\.|,]\\d{0,2})$");

    wColony[2].setTypeControl("ComboBox");
    wColony[2].setAssociatedParam("Cell_Type");
    wColony[6].setTypeControl("ComboBox");
    wColony[6].setAssociatedParam("UniParam");
    wColony[8].setTypeControl("ComboBox");
    wColony[8].setAssociatedParam("Colony_Glitter");
    wColony[9].setTypeControl("ComboBox");
    wColony[9].setAssociatedParam("Colony_Transparency");
    wColony[11].setTypeControl("ComboBox");
    wColony[11].setAssociatedParam("Colony_Surface");
    wColony[12].setTypeControl("ComboBox");
    wColony[12].setAssociatedParam("Colony_Profile");
    wColony[13].setTypeControl("CheckBox");
    wColony[13].setAssociatedParam("Colony_Growth");
    wColony[14].setTypeControl("ComboBox");
    wColony[14].setAssociatedParam("UniParam");
}


void StrainSheetForm::setBioChemStringList()   {
    QGridLayout *grid = static_cast<QGridLayout *>(ui->boxBioChem->layout());

    for (int i = 0; i < grid->rowCount()-1; i++)    {
        wBioChem[i].setID("BioChem");
        wBioChem[i].setViewWidget(grid->itemAtPosition(i, 1)->widget());
        wBioChem[i].setValidator("");
        wBioChem[i].setNotEmpty(false);
        wBioChem[i].setTypeControl("");
        wBioChem[i].setAssociatedParam("");
    }

    wBioChem[7].setTypeControl("CheckBox");
    wBioChem[7].setAssociatedParam("BioCh_Oxigen");
    wBioChem[8].setTypeControl("CheckBox");
    wBioChem[8].setAssociatedParam("BioCh_Feeding");

    wBioChem[1].setTypeControl("DoubleComboBox");
    wBioChem[1].setAssociatedParam("UniParam");
    wBioChem[2].setTypeControl("DoubleComboBox");
    wBioChem[2].setAssociatedParam("UniParam");
    wBioChem[3].setTypeControl("DoubleComboBox");
    wBioChem[3].setAssociatedParam("UniParam");
    wBioChem[6].setTypeControl("DoubleComboBox");
    wBioChem[6].setAssociatedParam("UniParam");
}



///////////////////////////////////////////////////////////////////////////////
void StrainSheetForm::setViewButtons(QGroupBox *box, IFormWidget *iwidget, bool NewItem)    {
    QGridLayout *grid = static_cast<QGridLayout *>(box->layout());
    QHBoxLayout *hLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(grid->rowCount()-1, 1)->layout());

    QPushButton *btnOK = static_cast<QPushButton *>(hLayout->itemAt(0)->widget());          // OK button
    QPushButton *btnAbort = static_cast<QPushButton *>(hLayout->itemAt(1)->widget());       // ABORT button
    QPushButton *btnEdit = static_cast<QPushButton *>(hLayout->itemAt(2)->widget());        // EDIT button
    btnOK->setVisible(false);
    btnAbort->setVisible(false);
    btnEdit->setVisible(true);
    grid->setAlignment(hLayout, Qt::AlignRight);

    disconnect(btnOK,       &QPushButton::clicked,      this,    0);
    disconnect(btnAbort,    &QPushButton::clicked,      this,    0);
    connect(btnOK,          &QPushButton::clicked,      this,   [=]()   {   applyChanges(iwidget, NewItem);   }   );
    connect(btnAbort,       &QPushButton::clicked,      this,   [=]()   {   abortChanges(iwidget, NewItem);   }   );
    connect(btnEdit,        &QPushButton::clicked,      this,   [=]()   {   editTable(iwidget);     }   );
}



void StrainSheetForm::setViewButtons(QGroupBox *box, bool NewItem)    {
    QGridLayout *grid = static_cast<QGridLayout *>(box->layout());
    QHBoxLayout *hLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(grid->rowCount()-1, 1)->layout());

    for (int i = 0; i < grid->rowCount()-1; i++)    {
        QVBoxLayout *cellLayout = static_cast<QVBoxLayout *>(grid->itemAtPosition(i, 1)->layout());
        static_cast<QTextEdit *>(cellLayout->itemAt(0)->widget())->setReadOnly(true);
        static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->setReadOnly(true);
        QHBoxLayout *btnLayout = static_cast<QHBoxLayout *>(cellLayout->itemAt(2)->layout());
        QPushButton *btnSet = static_cast<QPushButton *>(btnLayout->itemAt(0)->widget());       // SET button
        QPushButton *btnFile = static_cast<QPushButton *>(btnLayout->itemAt(1)->widget());      // FILE button
        QPushButton *btnDir = static_cast<QPushButton *>(btnLayout->itemAt(2)->widget());       // DIR button
        btnLayout->setAlignment(hLayout, Qt::AlignRight);
        btnSet->setVisible(false);
        btnFile->setVisible(true);
        btnDir->setVisible(true);
        if (static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->text().isEmpty())  {
            btnFile->setEnabled(false);
            btnDir->setEnabled(false);
        } else {
            btnFile->setEnabled(true);
            btnDir->setEnabled(true);
        }

//        connect(btnSet,     &QPushButton::clicked,  this,   [=]()   {   static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->setText(QFileDialog::getOpenFileName(this,
//                                                                                                                                  tr("Выбор файла"),
//                                                                                                                                  QCoreApplication::applicationDirPath()));  }   );
        disconnect(btnSet,      &QPushButton::clicked,      this,    0);
        disconnect(btnFile,     &QPushButton::clicked,      this,    0);
        disconnect(btnDir,      &QPushButton::clicked,      this,    0);
        connect(btnSet,         &QPushButton::clicked,  this,   [=]()   {   seqSetFile(static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget()));  }   );
        connect(btnFile,        &QPushButton::clicked,  this,   [=]()   {   seqFileOpen(static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->text());     }   );
        connect(btnDir,         &QPushButton::clicked,  this,   [=]()   {   seqDirOpen(static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->text());      }   );
    }

    QPushButton *btnOK = static_cast<QPushButton *>(hLayout->itemAt(0)->widget());          // OK button
    QPushButton *btnAbort = static_cast<QPushButton *>(hLayout->itemAt(1)->widget());       // ABORT button
    QPushButton *btnEdit = static_cast<QPushButton *>(hLayout->itemAt(2)->widget());        // EDIT button
    grid->setAlignment(hLayout, Qt::AlignRight);
    btnOK->setVisible(false);
    btnAbort->setVisible(false);
    btnEdit->setVisible(true);

    disconnect(btnOK,       &QPushButton::clicked,      this,    0);
    disconnect(btnAbort,    &QPushButton::clicked,      this,    0);
    connect(btnOK,          &QPushButton::clicked,      this,   [=]()   {   applyChanges(NewItem);  }   );
    connect(btnAbort,       &QPushButton::clicked,      this,   [=]()   {   abortChanges();         }   );
    connect(btnEdit,        &QPushButton::clicked,      this,   [=]()   {   editTable();            }   );
}


void StrainSheetForm::setBoxData(QGroupBox *box, IFormWidget *iwidget, QStringList &values)   {
    QGridLayout *grid = static_cast<QGridLayout *>(box->layout());

    for (int i = 0; i < grid->rowCount()-1; i++)    {
        iwidget[i].setViewValue(values[i]);
        iwidget[i].setStoredData(values[i]);
    }
}



void StrainSheetForm::setBoxData(QGroupBox *box, QStringList &values)   {
    QGridLayout *grid = static_cast<QGridLayout *>(box->layout());

    idSeq = static_cast<QString>(values[0]).toInt();
    listSeq.erase(listSeq.begin(), listSeq.end());
    for (int i = 0; i < grid->rowCount()-1; i++)    {
        QVBoxLayout *cellLayout = static_cast<QVBoxLayout *>(grid->itemAtPosition(i, 1)->layout());
        listSeq << values[2*i+1] << values[2*i+2];
        static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->setText(values[2*i+1]);
        static_cast<QTextEdit *>(cellLayout->itemAt(0)->widget())->setText(values[2*i+2]);
    }
    listSeq << values[7];
}



///////////////////////////////////////////////////////////////////////////////
void StrainSheetForm::seqSetFile(QLineEdit *edit)   {
    edit->setText(QFileDialog::getOpenFileName(this, tr("Выбор файла"), QCoreApplication::applicationDirPath()));
}


void StrainSheetForm::seqFileOpen(QString path)    {
    if (QFileInfo(path).exists() && QFileInfo(path).isFile()) {
            QDesktopServices::openUrl(QUrl::fromLocalFile(QDir(path).absolutePath()));
    } else {
        QMessageBox::information(0, tr("Ошибка"), tr("Файл не найден"));
    }
}


void StrainSheetForm::seqDirOpen(QString path) {
    if (QFileInfo(path).exists() && QFileInfo(path).isFile()) {
            QDesktopServices::openUrl(QUrl::fromLocalFile(QDir(path.chopped(path.length() - path.lastIndexOf('/'))).absolutePath()));
    } else {
        QMessageBox::information(0, tr("Ошибка"), tr("Файл не найден"));
    }
}



///////////////////////////////////////////////////////////////////////////////
void StrainSheetForm::fillEmptyBox(QGroupBox *box, IFormWidget *iwidget)    {
    QGridLayout *grid = static_cast<QGridLayout *>(box->layout());
    for (int i = 0; i < grid->rowCount()-1; i++)    {
        grid->itemAtPosition(i, 0)->widget()->hide();
        grid->itemAtPosition(i, 1)->widget()->hide();
    }

     QHBoxLayout *hLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(grid->rowCount()-1, 1)->layout());
     for (int i = 0; i < hLayout->count(); i++)
         hLayout->itemAt(i)->widget()->hide();

     QPushButton *btn = new QPushButton("Создать");
     hLayout->addWidget(btn);
     grid->setAlignment(hLayout, Qt::AlignRight);

     connect(btn,       &QPushButton::clicked,      this,   [=]()   {   createTable(iwidget->ID());     }   );
}



void StrainSheetForm::createTable(QString tableName) {
    IFormWidget *iwidget;
    QGridLayout *grid;
    if (tableName == "Strain")      {
        iwidget = wBase;
        grid = static_cast<QGridLayout *>(ui->boxBase->layout());
    } else if (tableName == "Colony")  {
        iwidget = wColony;
        grid = static_cast<QGridLayout *>(ui->boxColony->layout());
    } else if (tableName == "BioChem") {
        iwidget = wBioChem;
        grid = static_cast<QGridLayout *>(ui->boxBioChem->layout());
    }

    QHBoxLayout *hLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(grid->rowCount()-1, 1)->layout());
    delete static_cast<QPushButton *>(hLayout->itemAt(3)->widget());

    for (int i = 0; i < grid->rowCount()-1; i++)    {
        grid->itemAtPosition(i, 0)->widget()->show();
        grid->itemAtPosition(i, 1)->widget()->show();
    }

    int i;
    for (i = 1; i < grid->rowCount()-2; i++)    {
        iwidget[i].setEditable(true, i);
    }
    //iwidget[0].setViewValue(newID);
    if (tableName == "Strain")
        iwidget[i].setEditable(true, i);
    else
        iwidget[i].setViewValue(id);              //iwidget[i].setViewValue(wBase[0].getWidgetData());

    grid->setAlignment(hLayout, Qt::AlignLeft);
    static_cast<QPushButton *>(hLayout->itemAt(0)->widget())->setVisible(true);         // OK button
    static_cast<QPushButton *>(hLayout->itemAt(1)->widget())->setVisible(true);         // ABORT button
    static_cast<QPushButton *>(hLayout->itemAt(2)->widget())->setVisible(false);        // EDIT button
}

void StrainSheetForm::setID(QString tableName, int _id)  {
    if (tableName == "Strain")  {
        wBase[0].setViewValue(QString::number(_id));
        wBase[0].setStoredData(QString::number(_id));
        id = QString::number(_id);
    } else if (tableName == "Colony")   {
        wColony[0].setViewValue(QString::number(_id));
        wColony[0].setStoredData(QString::number(_id));
    } else if (tableName == "BioChem") {
        wBioChem[0].setViewValue(QString::number(_id));
        wBioChem[0].setStoredData(QString::number(_id));
    }
}



///////////////////////////////////////////////////////////////////////////////
void StrainSheetForm::editTable(IFormWidget *iwidget)   {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QHBoxLayout *hLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(grid->rowCount()-1, 1)->layout());

    grid->setAlignment(hLayout, Qt::AlignLeft);
    static_cast<QPushButton *>(hLayout->itemAt(0)->widget())->setVisible(true);         // OK button
    static_cast<QPushButton *>(hLayout->itemAt(1)->widget())->setVisible(true);         // ABORT button
    static_cast<QPushButton *>(hLayout->itemAt(2)->widget())->setVisible(false);        // EDIT button

    int i;
    for(i = 1; i < (grid->rowCount()-2); i++)
        iwidget[i].setEditable(true, i);
    if (iwidget == wBase)
        iwidget[i].setEditable(true, i);

//    grid->itemAtPosition(1, 1)->widget()->setFocus();
}


void StrainSheetForm::applyChanges(IFormWidget *iwidget, bool NewItem)    {
    QGridLayout *grid = static_cast<QGridLayout*>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QHBoxLayout *hLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(grid->rowCount()-1, 1)->layout());

    for (int i = 1; i < (grid->rowCount()-1); i++)  {
        if (iwidget[i].isNotEmpty() && static_cast<QLineEdit *>(iwidget[i].viewWidget())->text().isEmpty()) {
            iwidget[i].viewWidget()->setFocus();
            return;
        }
    }

    QStringList listValues;
    //int i = NewItem ? 1 : 0;
    QString tableID;
    for (int i = 0; i < grid->rowCount()-1; i++)    {
        iwidget[i].setEditable(false, i);
        iwidget[i].setStoredData(iwidget[i].getWidgetData());
        if (i == 0)
            tableID = iwidget[i].getWidgetData();
        else listValues << iwidget[i].getWidgetData();
    }

    if (!NewItem)    {
        grid->setAlignment(hLayout, Qt::AlignRight);
        static_cast<QPushButton *>(hLayout->itemAt(0)->widget())->setVisible(false);        // OK button
        static_cast<QPushButton *>(hLayout->itemAt(1)->widget())->setVisible(false);        // ABORT button
        static_cast<QPushButton *>(hLayout->itemAt(2)->widget())->setVisible(true);         // EDIT button
        emit strainDataChanged(listValues, tableID, id, iwidget->ID());
        return;
    }

    setViewButtons(static_cast<QGroupBox *>(sender()->parent()), iwidget, false);
    emit strainDataAdded(listValues, iwidget->ID());
    emit strainNewItem(this, iwidget->ID());

    if (iwidget->ID() == "Strain")    {
        setViewButtonsDuo(ui->boxAntiBio);
        setViewButtonsDuo(ui->boxSubstrats);
        setBoxDataDuo(ui->boxAntiBio, mAntiBio);
        setBoxDataDuo(ui->boxSubstrats, mSubstrats);
    }
}


void StrainSheetForm::abortChanges(IFormWidget *iwidget, bool NewItem)  {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QHBoxLayout *hLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(grid->rowCount()-1, 1)->layout());

    for (int i = 0; i < grid->rowCount()-1; i++)    {
        iwidget[i].setEditable(false, i);
        iwidget[i].setViewValue(iwidget[i].storedData());
    }
    if (!NewItem)   {
        grid->setAlignment(hLayout, Qt::AlignRight);
        static_cast<QPushButton *>(hLayout->itemAt(0)->widget())->setVisible(false);        // OK button
        static_cast<QPushButton *>(hLayout->itemAt(1)->widget())->setVisible(false);        // ABORT button
        static_cast<QPushButton *>(hLayout->itemAt(2)->widget())->setVisible(true);         // EDIT button
        return;
    }
    fillEmptyBox(static_cast<QGroupBox *>(sender()->parent()), iwidget);
}


void StrainSheetForm::editTable()   {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QHBoxLayout *hLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(grid->rowCount()-1, 1)->layout());

    for (int i = 0; i < grid->rowCount()-1; i++)    {
        QVBoxLayout *cellLayout = static_cast<QVBoxLayout *>(grid->itemAtPosition(i, 1)->layout());
        static_cast<QTextEdit *>(cellLayout->itemAt(0)->widget())->setReadOnly(false);
        static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->setReadOnly(false);
        QHBoxLayout *btnLayout = static_cast<QHBoxLayout *>(cellLayout->itemAt(2)->layout());
        static_cast<QPushButton *>(btnLayout->itemAt(0)->widget())->setVisible(true);       // SET button
        static_cast<QPushButton *>(btnLayout->itemAt(1)->widget())->setVisible(false);      // FILE button
        static_cast<QPushButton *>(btnLayout->itemAt(2)->widget())->setVisible(false);      // DIR button
    }
    static_cast<QPushButton *>(hLayout->itemAt(0)->widget())->setVisible(true);             // OK button
    static_cast<QPushButton *>(hLayout->itemAt(1)->widget())->setVisible(true);             // ABORT button
    static_cast<QPushButton *>(hLayout->itemAt(2)->widget())->setVisible(false);            // EDIT button
    grid->setAlignment(hLayout, Qt::AlignLeft);

    QVBoxLayout *cellLayout = static_cast<QVBoxLayout *>(grid->itemAtPosition(0, 1)->layout());
    QTextEdit *edit = static_cast<QTextEdit *>(cellLayout->itemAt(0)->widget());
    edit->setFocus();
}


void StrainSheetForm::applyChanges(bool NewItem)    {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QHBoxLayout *hLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(grid->rowCount()-1, 1)->layout());

    for (int i = 0; i < grid->rowCount()-1; i++)    {
        QVBoxLayout *cellLayout = static_cast<QVBoxLayout *>(grid->itemAtPosition(i, 1)->layout());
        listSeq[2*i] = static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->text();
        listSeq[2*i+1] = static_cast<QTextEdit *>(cellLayout->itemAt(0)->widget())->toPlainText();
        QHBoxLayout *btnLayout = static_cast<QHBoxLayout *>(cellLayout->itemAt(2)->layout());
        static_cast<QTextEdit *>(cellLayout->itemAt(0)->widget())->setReadOnly(true);
        static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->setReadOnly(true);
        static_cast<QPushButton *>(btnLayout->itemAt(0)->widget())->setVisible(false);      // SET button
        static_cast<QPushButton *>(btnLayout->itemAt(1)->widget())->setVisible(true);       // FILE button
        static_cast<QPushButton *>(btnLayout->itemAt(2)->widget())->setVisible(true);       // DIR button
        if (static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->text().isEmpty())  {
            static_cast<QPushButton *>(btnLayout->itemAt(1)->widget())->setEnabled(false);
            static_cast<QPushButton *>(btnLayout->itemAt(2)->widget())->setEnabled(false);
        } else {
            static_cast<QPushButton *>(btnLayout->itemAt(1)->widget())->setEnabled(true);
            static_cast<QPushButton *>(btnLayout->itemAt(2)->widget())->setEnabled(true);
        }
    }
    static_cast<QPushButton *>(hLayout->itemAt(0)->widget())->setVisible(false);            // OK button
    static_cast<QPushButton *>(hLayout->itemAt(1)->widget())->setVisible(false);            // ABORT button
    static_cast<QPushButton *>(hLayout->itemAt(2)->widget())->setVisible(true);             // EDIT button
    grid->setAlignment(hLayout, Qt::AlignRight);

    if (NewItem)    {
        listSeq[6] = id;
//        QStringList newValues;
//        for (int i = 1; i < listSeq.size(); i++)
//            newValues << listSeq[i];
        //qDebug() << newValues;
        emit strainDataAdded(listSeq, "Seq");
        setViewButtons(static_cast<QGroupBox *>(sender()->parent()), false);
    }
    else
        emit strainDataChanged(listSeq, idSeq, id, "Seq");


}


void StrainSheetForm::abortChanges()  {
    QGridLayout *grid = static_cast<QGridLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QHBoxLayout *hLayout = static_cast<QHBoxLayout *>(grid->itemAtPosition(grid->rowCount()-1, 1)->layout());

    for (int i = 0; i < grid->rowCount()-1; i++)    {
        QVBoxLayout *cellLayout = static_cast<QVBoxLayout *>(grid->itemAtPosition(i, 1)->layout());
        static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->setText(listSeq[2*i]);
        static_cast<QTextEdit *>(cellLayout->itemAt(0)->widget())->setText(listSeq[2*i+1]);
        QHBoxLayout *btnLayout = static_cast<QHBoxLayout *>(cellLayout->itemAt(2)->layout());
        static_cast<QTextEdit *>(cellLayout->itemAt(0)->widget())->setReadOnly(true);
        static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->setReadOnly(true);
        static_cast<QPushButton *>(btnLayout->itemAt(0)->widget())->setVisible(false);      // SET button
        static_cast<QPushButton *>(btnLayout->itemAt(1)->widget())->setVisible(true);       // FILE button
        static_cast<QPushButton *>(btnLayout->itemAt(2)->widget())->setVisible(true);       // DIR button
        if (static_cast<QLineEdit *>(cellLayout->itemAt(1)->widget())->text().isEmpty())  {
            static_cast<QPushButton *>(btnLayout->itemAt(1)->widget())->setEnabled(false);
            static_cast<QPushButton *>(btnLayout->itemAt(2)->widget())->setEnabled(false);
        } else {
            static_cast<QPushButton *>(btnLayout->itemAt(1)->widget())->setEnabled(true);
            static_cast<QPushButton *>(btnLayout->itemAt(2)->widget())->setEnabled(true);
        }
    }
    static_cast<QPushButton *>(hLayout->itemAt(0)->widget())->setVisible(false);            // OK button
    static_cast<QPushButton *>(hLayout->itemAt(1)->widget())->setVisible(false);            // ABORT button
    static_cast<QPushButton *>(hLayout->itemAt(2)->widget())->setVisible(true);             // EDIT button
    grid->setAlignment(hLayout, Qt::AlignRight);
}


///////////////////////////////////////////////////////////////////////////////
void StrainSheetForm::setBoxDataDuo(QGroupBox *box, BartProxyModel *model)  {
    QHBoxLayout * layout = static_cast<QHBoxLayout *>(box->layout());
    QTableView * view = static_cast<QTableView *>(layout->itemAt(0)->widget());

    model->setFilter(QString("%1 = %2").arg(model->record().fieldName(4), id));
    model->select();
//qDebug() << model->lastError();
//qDebug() << model->query().lastQuery();
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->setHeaderData(1, Qt::Horizontal, "Название");
    model->setHeaderData(2, Qt::Horizontal, "Реакция");
    model->setHeaderData(3, Qt::Horizontal, "Комментарий");

    view->setModel(model);
    view->setEditTriggers(QAbstractItemView::NoEditTriggers);
    view->setSortingEnabled(true);
    view->horizontalHeader()->setSortIndicator(1, Qt::AscendingOrder);
    view->resizeColumnsToContents();
    view->horizontalHeader()->setSectionHidden(0, true);
    view->horizontalHeader()->setSectionHidden(4, true);
    view->horizontalHeader()->setStretchLastSection(true);
}


void StrainSheetForm::setViewButtonsDuo(QGroupBox *box)    {
    QHBoxLayout * layout = static_cast<QHBoxLayout *>(box->layout());
    QHBoxLayout * hLayoutLeft = static_cast<QHBoxLayout *>(layout->itemAt(1)->layout());
    QHBoxLayout * hLayoutRight = static_cast<QHBoxLayout *>(layout->itemAt(2)->layout());

    QPushButton * btnEdit = static_cast<QPushButton *>(hLayoutRight->itemAt(0)->widget());       // EDIT button
    QPushButton * btnOK = static_cast<QPushButton *>(hLayoutRight->itemAt(1)->widget());         // OK button
    QPushButton * btnAbort = static_cast<QPushButton *>(hLayoutRight->itemAt(2)->widget());      // ABORT button
    QPushButton * btnAdd = static_cast<QPushButton *>(hLayoutLeft->itemAt(0)->widget());         // ADD button
    QPushButton * btnDel = static_cast<QPushButton *>(hLayoutLeft->itemAt(1)->widget());         // DEL button
    QPushButton * btnExport = static_cast<QPushButton *>(hLayoutLeft->itemAt(2)->widget());      // EXPORT button
    QPushButton * btnImport = static_cast<QPushButton *>(hLayoutLeft->itemAt(3)->widget());      // IMPORT button
    QPushButton * btnUpdate = static_cast<QPushButton *>(hLayoutLeft->itemAt(4)->widget());      // UPDATE button

    layout->setAlignment(hLayoutLeft, Qt::AlignTop);
    layout->setAlignment(hLayoutRight, Qt::AlignTop);
    btnEdit->setVisible(true);
    btnOK->setVisible(false);
    btnAbort->setVisible(false);
    btnAdd->setVisible(false);
    btnDel->setVisible(false);
    btnExport->setVisible(false);
    btnImport->setVisible(false);
    btnUpdate->setVisible(false);

    if(id.toInt())
        btnEdit->setEnabled(true);
    else
        btnEdit->setEnabled(false);

    connect(btnEdit,        &QPushButton::clicked,      this,   [=]()   {   editTableDuo();     }   );
    connect(btnOK,          &QPushButton::clicked,      this,   [=]()   {   applyChangesDuo();  }   );
    connect(btnAbort,       &QPushButton::clicked,      this,   [=]()   {   abortChangesDuo();  }   );
    connect(btnAdd,         &QPushButton::clicked,      this,   [=]()   {   addRow();           }   );
    connect(btnDel,         &QPushButton::clicked,      this,   [=]()   {   delRow();           }   );
    connect(btnExport,      &QPushButton::clicked,      this,       &StrainSheetForm::exportData);
    connect(btnImport,      &QPushButton::clicked,      this,       &StrainSheetForm::importData);
    connect(btnUpdate,      &QPushButton::clicked,      this,       &StrainSheetForm::updateData);
}


void StrainSheetForm::editTableDuo()   {
    QHBoxLayout * layout = static_cast<QHBoxLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QTableView * view = static_cast<QTableView *>(layout->itemAt(0)->widget());
    QHBoxLayout * hLayoutLeft = static_cast<QHBoxLayout *>(layout->itemAt(1)->layout());
    QHBoxLayout * hLayoutRight = static_cast<QHBoxLayout *>(layout->itemAt(2)->layout());

    static_cast<QPushButton *>(hLayoutRight->itemAt(0)->widget())->setVisible(false);       // EDIT button
    static_cast<QPushButton *>(hLayoutRight->itemAt(1)->widget())->setVisible(true);        // OK button
    static_cast<QPushButton *>(hLayoutRight->itemAt(2)->widget())->setVisible(true);        // ABORT button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(0)->widget())->setVisible(true);         // ADD button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(1)->widget())->setVisible(true);         // DEL button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(2)->widget())->setVisible(true);         // EXPORT button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(3)->widget())->setVisible(true);         // IMPORT button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(4)->widget())->setVisible(true);         // UPDATE button

    view->setEditTriggers(QAbstractItemView::DoubleClicked);
    setTabOrder(hLayoutRight->itemAt(0)->widget(), layout->itemAt(0)->widget());
}


void StrainSheetForm::applyChangesDuo()   {
    QHBoxLayout * layout = static_cast<QHBoxLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QTableView * view = static_cast<QTableView *>(layout->itemAt(0)->widget());
    QHBoxLayout * hLayoutLeft = static_cast<QHBoxLayout *>(layout->itemAt(1)->layout());
    QHBoxLayout * hLayoutRight = static_cast<QHBoxLayout *>(layout->itemAt(2)->layout());

    static_cast<QPushButton *>(hLayoutRight->itemAt(0)->widget())->setVisible(true);        // EDIT button
    static_cast<QPushButton *>(hLayoutRight->itemAt(1)->widget())->setVisible(false);       // OK button
    static_cast<QPushButton *>(hLayoutRight->itemAt(2)->widget())->setVisible(false);       // ABORT button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(0)->widget())->setVisible(false);        // ADD button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(1)->widget())->setVisible(false);        // DEL button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(2)->widget())->setVisible(false);        // EXPORT button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(3)->widget())->setVisible(false);        // IMPORT button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(4)->widget())->setVisible(false);        // UPDATE button

    view->setEditTriggers(QAbstractItemView::NoEditTriggers);
    setTabOrder(hLayoutRight->itemAt(1)->widget(), layout->itemAt(0)->widget());

    static_cast<BartProxyModel *>(view->model())->submitAll();
}


void StrainSheetForm::abortChangesDuo()   {
    QHBoxLayout * layout = static_cast<QHBoxLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QTableView * view = static_cast<QTableView *>(layout->itemAt(0)->widget());
    QHBoxLayout * hLayoutLeft = static_cast<QHBoxLayout *>(layout->itemAt(1)->layout());
    QHBoxLayout * hLayoutRight = static_cast<QHBoxLayout *>(layout->itemAt(2)->layout());

    static_cast<QPushButton *>(hLayoutRight->itemAt(0)->widget())->setVisible(true);        // EDIT button
    static_cast<QPushButton *>(hLayoutRight->itemAt(1)->widget())->setVisible(false);       // OK button
    static_cast<QPushButton *>(hLayoutRight->itemAt(2)->widget())->setVisible(false);       // ABORT button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(0)->widget())->setVisible(false);        // ADD button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(1)->widget())->setVisible(false);        // DEL button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(2)->widget())->setVisible(false);        // EXPORT button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(3)->widget())->setVisible(false);        // IMPORT button
    static_cast<QPushButton *>(hLayoutLeft->itemAt(4)->widget())->setVisible(false);        // UPDATE button

    view->setEditTriggers(QAbstractItemView::NoEditTriggers);
    setTabOrder(hLayoutRight->itemAt(2)->widget(), layout->itemAt(0)->widget());

    static_cast<BartProxyModel *>(view->model())->revertAll();
}


void StrainSheetForm::addRow()    {
    QHBoxLayout * layout = static_cast<QHBoxLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QTableView * view = static_cast<QTableView *>(layout->itemAt(0)->widget());
    BartProxyModel * model = static_cast<BartProxyModel *>(view->model());

    QSqlRecord record = model->record();
    record.remove(0);
    record.setValue(3, id);

    if (model->rowCount())
        model->insertRecord(-1, record);
    else
        model->insertRecord(0, record);

    view->selectRow(model->rowCount() - 1);
    view->setFocus();
}


void StrainSheetForm::delRow()    {
    QHBoxLayout * layout = static_cast<QHBoxLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QTableView * view = static_cast<QTableView *>(layout->itemAt(0)->widget());

    view->model()->removeRow(view->currentIndex().row());
}


void StrainSheetForm::exportData()  {
    QHBoxLayout * layout = static_cast<QHBoxLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QTableView * view = static_cast<QTableView *>(layout->itemAt(0)->widget());
    static_cast<BartProxyModel *>(view->model())->exportToFile();
}


void StrainSheetForm::importData()  {
    QHBoxLayout * layout = static_cast<QHBoxLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QTableView * view = static_cast<QTableView *>(layout->itemAt(0)->widget());
    static_cast<BartProxyModel *>(view->model())->importFromFile();
}


void StrainSheetForm::updateData()  {
    QHBoxLayout * layout = static_cast<QHBoxLayout *>(static_cast<QGroupBox *>(sender()->parent())->layout());
    QTableView * view = static_cast<QTableView *>(layout->itemAt(0)->widget());
    static_cast<BartProxyModel *>(view->model())->updateFromFile();
}


