#ifndef IFORMWIDGET_H
#define IFORMWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QTextEdit>
#include <QDateEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QGridLayout>
#include <QRegExpValidator>
#include <QGroupBox>
#include <QSettings>
#include <QDebug>

class IFormWidget   :   public QObject
{
    Q_OBJECT
//    Q_PROPERTY(QString typeControl READ typeControl WRITE setTypeControl)
//    Q_PROPERTY(bool notEmpty READ isNotEmpty WRITE setNotEmpty)

public:
    explicit IFormWidget(QObject *parent = nullptr);
    IFormWidget(QWidget *view, QString type, QObject *parent = nullptr);
    IFormWidget(QWidget *view, QWidget *control, QObject *parent = nullptr);
    ~IFormWidget();

    QWidget *viewWidget()                   {   return wView;           }
    void setViewWidget(QWidget *w)          {   wView = w;
                                                if (qobject_cast<QTextEdit *>(wView))
                                                    qobject_cast<QTextEdit *>(wView)->setTabChangesFocus(true);     }

    QWidget *controlWidget()                {   return wControl;        }
    void setControlWidget(QWidget *w)       {   wControl = w;           }

    QString typeControl()                   {   return _typeControl;    }
    void setTypeControl(QString str)        {   _typeControl = str;     }

    bool isNotEmpty()                       {   return _notEmpty;       }
    void setNotEmpty(bool value)            {   _notEmpty = value;      }

    QString storedData()                    {   return _storedData;     }
    void setStoredData(QString str)         {   _storedData = str;      }

    QString associatedParam()               {   return _param;          }
    void setAssociatedParam(QString str)    {   _param = str;           }

    QString ID()                            {   return _idName;         }
    void setID(QString val)                 {   _idName = val;          }

    void setViewValue(QString str);
    void setValidator(QString strRegExp);
    void setEditable(bool edit, int row);
    QString getWidgetData();

signals:

public slots:

private:
    void LineEditToCheckBox(int row);
    void CheckBoxToLineEdit(int row);
    void LineEditToComboBox(int row);
    void ComboBoxToLineEdit(int row);
    void LineEditToDoubleComboBox(int row);
    void DoubleComboBoxToLineEdit(int row);

    QWidget *       wControl;
    QWidget *       wView;
    QString         _typeControl;
    bool            _notEmpty;
    bool            _editable;
    QString         _storedData;
    QString         _param;
    QString         _idName;
};

#endif // IFORMWIDGET_H
