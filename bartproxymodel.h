#ifndef BARTPROXYMODEL_H
#define BARTPROXYMODEL_H

#include <QObject>
#include <QtSql/QtSql>
#include <QBrush>
#include <QFont>

#include <QMessageBox>
#include <QFileDialog>

#include <QAxObject>
#include <QAxWidget>

#include <QDebug>

class BartProxyModel : public QSqlTableModel
{
    Q_OBJECT

public:
    BartProxyModel();
    BartProxyModel(QObject *parent = nullptr, QSqlDatabase db = QSqlDatabase());

//    void insertRow();
//    void insertRowData(QStringList &listData);
//    void updateRowData(int row, QSqlRecord record);

    int findDatainRow(QString Name);
    int findDuplicate(QString Name, int foreignID);
    void exportToFile();
    void importFromFile();
    void updateFromFile();

//    bool submitAll()        {   return  static_cast<QSqlTableModel *>(this->sourceModel())->submitAll();    }
//    void revertAll()        {   static_cast<QSqlTableModel *>(this->sourceModel())->revertAll();            }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};

#endif // BARTPROXYMODEL_H
