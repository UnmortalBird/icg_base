#ifndef GROUPEDITPARAMDIALOG_H
#define GROUPEDITPARAMDIALOG_H

#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QCheckBox>
#include <QPushButton>
#include <QSettings>
#include <QTextCodec>
#include <QDebug>

namespace Ui {
class GroupEditParamDialog;
}

class GroupEditParamDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GroupEditParamDialog(QWidget *parent = 0);
    ~GroupEditParamDialog();

    QString fields();

private:
    Ui::GroupEditParamDialog    *ui;
    QVBoxLayout                 *mainLayout;
    QHBoxLayout                 *btnLayout;
    QVBoxLayout                 *chbLayout;
    QPushButton                 *btnOK;
    QPushButton                 *btnCancel;
};

#endif // GROUPEDITPARAMDIALOG_H
