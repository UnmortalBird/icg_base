#include "tablefieldsdialog.h"
#include "ui_tablefieldsdialog.h"

TableFieldsDialog::TableFieldsDialog(QStringList list, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TableFieldsDialog)
{
    ui->setupUi(this);
    setWindowTitle("Выбор отображаемых данных");
    //vLayout = new QVBoxLayout;
    gLayout = new QGridLayout;
    hLayout = new QHBoxLayout;
    btnLayout = new QVBoxLayout;
    btnOK = new QPushButton("OK");
    btnCancel = new QPushButton("Отмена");
    btnLayout->addWidget(btnOK);
    btnLayout->addWidget(btnCancel);

    QSettings settings("settings.ini", QSettings::IniFormat);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    settings.setIniCodec(codec);

    int colCount = (list.count() + 14) / 15;
    int lastColumn = list.count() - (15 * (colCount-1));
    for (int i = 0; i < colCount; i++) {
        for (int j = 0; j < 15; j++)  {
            if (j && (i == colCount - 1) && (j == lastColumn)) break;
            gLayout->addWidget(new QCheckBox(list[15*i + j]), j, i);
            if (settings.value("Fields/TableFields").toString().at(15*i + j) == '1')
                static_cast<QCheckBox *>(gLayout->itemAtPosition(j, i)->widget())->setChecked(true);
        }
    }
    hLayout->addLayout(gLayout);
    hLayout->addLayout(btnLayout);
    setLayout(hLayout);
    connect(btnOK,      &QPushButton::clicked,      this,       &TableFieldsDialog::accept);
    connect(btnCancel,  &QPushButton::clicked,      this,       &TableFieldsDialog::reject);
}

TableFieldsDialog::~TableFieldsDialog()
{
    delete ui;
    delete gLayout;
    delete btnLayout;
    delete hLayout;
    delete btnOK;
    delete btnCancel;
}


QStringList TableFieldsDialog::fields() {
    QStringList list;
    for (int i = 0; i < gLayout->columnCount(); i++)    {
        for (int j = 0; j < gLayout->rowCount(); j++)       {
            if ((gLayout->rowCount()*i + j) == gLayout->count()) break;
            if (static_cast<QCheckBox *>(gLayout->itemAtPosition(j, i)->widget())->isChecked())
                list << static_cast<QCheckBox *>(gLayout->itemAtPosition(j, i)->widget())->text();
        }
    }

    return list;
}

