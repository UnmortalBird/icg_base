#ifndef SECONDARYWINDOW_H
#define SECONDARYWINDOW_H

#include <QMainWindow>
#include <QSqlTableModel>
#include <QtSql/QtSql>
#include <QDebug>

#include "bartproxymodel.h"
#include <QMessageBox>
#include <QFileDialog>

namespace Ui {
class SecondaryWindow;
}

class SecondaryWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SecondaryWindow(QString windowName, BartProxyModel *secModel, QWidget *parent = 0);
    ~SecondaryWindow();

    void importData()   {   model->importFromFile();    }
    void updateData()   {   model->updateFromFile();    }

private:
    void initMenu();
    void setTableView();
    void search();
//    void sumTable();
    void addRecord();
    void delRecord();

    void applyChanges();
    void abortChanges();

//    void selectHeader(int logicalIndex);

    Ui::SecondaryWindow     *ui;
    BartProxyModel          *model;
};

#endif // SECONDARYWINDOW_H
