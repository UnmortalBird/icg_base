#ifndef DBPATHESDIALOG_H
#define DBPATHESDIALOG_H

#include <QDialog>
#include <QSettings>
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>

namespace Ui {
class DBPathesDialog;
}

class DBPathesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DBPathesDialog(QWidget *parent = 0);
    ~DBPathesDialog();

    QString strainPath() const;
//    QString colonyPath() const;
//    QString bioChemPath() const;
//    QString seqPath() const;
//    QString antiBioPath() const;
//    QString substratsPath() const;

private slots:
    void strainButtonClicked();
//    void colonyButtonClicked();
//    void bioChemButtonClicked();
//    void seqButtonClicked();
//    void antiBioButtonClicked();
//    void substratsButtonClicked();
    void okButtonClicked();

private:
    Ui::DBPathesDialog *ui;
};

#endif // DBPATHESDIALOG_H
