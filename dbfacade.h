#ifndef DBFACADE_H
#define DBFACADE_H

#include <QObject>
#include <QtSql/QtSql>
#include <QTableView>
#include <QHeaderView>
#include <QVector>
#include <QMessageBox>
#include <QDebug>

#include "bartproxymodel.h"

class QTableView;
class QSqlTableModel;
class QSqlQueryModel;

class DBFacade : public QObject
{
    Q_OBJECT
public:
    explicit DBFacade(const QSqlDatabase &database, QObject *parent = nullptr);
    virtual ~DBFacade();

    void                    search(QString value, QString param);
    QStringList             searchID(QString value, QString param);
//    void                    setFields(QList<QPair<QString, QString> > list);
    void                    sort(QString header, Qt::SortOrder order = Qt::AscendingOrder);    // override;
    void                    update();

    QVector<QStringList>    rowValueVector(const QModelIndex &index);
    QVector<QStringList>    rowValueVector(const int id);

    int                     lastID(QString tableName);
    void                    addItem(QString tableName, QStringList &values);
    void                    editItem(QString tableName, int idx, QStringList &values);
    void                    delItem(QModelIndexList list);

    QSqlQueryModel          *mainModel()                                    {   return m_model;     }
    BartProxyModel          *getSecondaryModel(QString tableName, int id);
//    BartProxyModel          *getSecondaryModel(QString tableName, const QModelIndex & index);

    void                    backupAll();
    void                    restoreAll();
    void                    importBaseFromFile();
    void                    updateBaseFromFile();
    void                    importNotBaseFromFile(QString tableName, QString uniqueField);
    void                    updateNotBaseFromFile(QString tableName, QString uniqueField);

//    void          setDB(const QSqlDatabase &database)             {   m_db = database;        }

    void                    setPathStrain(QString str)                      {   pathStrain = str;       }
//    void                    setPathColony(QString str)                      {   pathColony = str;       }
//    void                    setPathBioChem(QString str)                     {   pathBioChem = str;      }
//    void                    setPathSeq(QString str)                         {   pathSeq = str;          }
//    void                    setPathAntiBio(QString str)                     {   pathAntiBio = str;      }
//    void                    setPathSubstrats(QString str)                   {   pathSubstrats = str;    }
//    void            setTableName(const QString &name)               {   m_name = name;          }
//    QString         tableName()                                     {   return this->m_name;    }


//    void addFake();
//    void delFake();
signals:

public slots:

protected:
  void                      exec(QString);      // Execute the query
  QString                   qs(QString);
  QStringList               rowValueList(QString tableName, QString header, int value);
  void                      backupTable(QString fileName, QString tableName);
  void                      restoreTable(QString fileName, QString tableName);
  void                      eraseTable(QString tableName);
  QSqlDatabase              m_db;               // DataBase
  QTableView                *m_view;
  QSqlQueryModel            *m_model;           // Working model
  BartProxyModel            *m_second_1;        // Secondary model
  BartProxyModel            *m_second_2;        // Secondary model
//  QSqlTableModel            *m_second_3;        // Secondary model
//  QSqlQuery             *m_query;             // DB query
//  QSqlRecord            m_rec;                // Row of the table (exec answer)

  Qt::SortOrder             sortOrder;
  QString                   sortHeader;

  QString                   pathStrain;
//  QString                   pathColony;
//  QString                   pathBioChem;
//  QString                   pathSeq;
//  QString                   pathAntiBio;
//  QString                   pathSubstrats;
};

#endif // DBFACADE_H
