#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlTableModel>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QDebug>
#include <QSettings>
#include <QString>
#include <QStringList>
#include <QTextCodec>
#include <QMap>
#include <QPrinter>
#include <QTextDocumentWriter>
#include <QtSql/QtSql>
#include <QMessageBox>

#include <qaxobject.h>
#include <qaxwidget.h>
//#include <QSqlRelationalTableModel>
//#include <QFileDialog>
//#include <QWindow>
//#include <QDir>
//#include <QFileInfoList>

#include "dbfacade.h"
#include "sqlconnectiondialog.h"
#include "dbpathesdialog.h"
#include "valueslistdialog.h"
//#include "bartsqlquerymodel.h"
#include "strainsheetform.h"
#include "tablefieldsdialog.h"
#include "secondarywindow.h"
#include "groupeditparamdialog.h"

namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    virtual void keyPressEvent(QKeyEvent *event) override;

private slots:
    void add();
//    void edit();
    void del();
//    void view();
    void    view(const QModelIndex &index);
    void    search();
    void    header_clicked(int logicalIndex);
    void    strainSheetNewItemID(StrainSheetForm * sheet, QString tableName);
    void    strainSheetNewItem(QStringList &listValues, QString tableName);
    void    strainSheetDataEdited(QStringList listValues, QString tableID, QString strainID, QString tableName);

//    void    strainSheetNewGroupItemID(StrainSheetForm * sheet, QString tableName);
    void    strainSheetNewGroupItem(QStringList &listValues, QString tableName);
    void    strainSheetGroupDataEdited(QStringList listValuesGroup, QString id, QString strainID, QString tableName);

private:
    void    initMenu();
//    void    initToolBar();
    void    initFields();
    void    initSettings();
    void    setActualFields();
    void    setActiveElements();

    void    setTableView();
    void    groupEditSel();
    void    groupEditParam();
    void    groupView(QStringList listID);
    void    connectDB();
    void    pathesDB();
    void    openDB();
//    void    exportDB()            {    mFacade->backupAll();   }
//    void    importDB()            {    mFacade->restoreAll();   }
    void    setFields();
    void    setValuesList(QString groupName, QString itemName, QString dialogName);

    void    exportTable(bool allTable = true);
    void    exportXLS(QString fileName, bool allTable);
    void    exportHTML(QString fileName, bool allTable);
    void    exportPDF(QString fileName, bool allTable);
    QString getHTMLtoExport(bool allTable);

    void    sumTable(BartProxyModel *secModel);
//    void    exportODT();


    Ui::MainWindow                  *ui;
    QSqlDatabase                    db;
    QList<QPair<QString, QString>>  fields;

    DBFacade                        *mFacade;
    SecondaryWindow                 *antibioWindow;
    SecondaryWindow                 *substratsWindow;
};

#endif // MAINWINDOW_H
