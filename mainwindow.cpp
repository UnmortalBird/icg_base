#include "mainwindow.h"
#include "ui_mainwindow.h"

//#include <QItemDelegate>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);

    this->setWindowTitle("База штаммов ИЦиГ");

    ui->cBoxSrch->addItem("Название");
    ui->cBoxSrch->addItem("Место выделения");
    ui->cBoxSrch->addItem("Таксономия");
    ui->cBoxSrch->addItem("Номер в ген. банке");
    ui->cBoxSrch->addItem("Группа");

    initFields();
    initSettings();
    initMenu();
//    initToolBar();
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.setIniCodec(codec);
    db = QSqlDatabase::addDatabase(settings.value("DatabaseConnection/Driver").toString(),
                                   settings.value("DatabaseConnection/ConnectionName").toString());
    openDB();

    mFacade = new DBFacade(db, this);
    ui->tableView->setModel(mFacade->mainModel());

    setActualFields();
    setTableView();

    mFacade->setPathStrain(settings.value("Path/BackupPath").toString());
    antibioWindow = new SecondaryWindow("Антибиотики", mFacade->getSecondaryModel("AntiBio", 0));
    substratsWindow = new SecondaryWindow("Биохимия", mFacade->getSecondaryModel("Substrats", 0));

    setActiveElements();

    connect(ui->btnSrch,                        &QPushButton::clicked,              this,                   &MainWindow::search);
    connect(ui->lineSrch,                       &QLineEdit::returnPressed,          this,                   &MainWindow::search);
    connect(ui->tableView->horizontalHeader(),  &QHeaderView::sectionClicked,       this,                   &MainWindow::header_clicked);
    connect(ui->tableView,                      &QTableView::doubleClicked,         this,                   &MainWindow::view);
//    connect(ui->btnAntiBio,                     &QPushButton::clicked,              this->antibioWindow,    &SecondaryWindow::show   );
//    connect(ui->btnSubStrats,                   &QPushButton::clicked,              this->substratsWindow,  &SecondaryWindow::show   );
//    connect(ui->btnSumTblAntiBio,               &QPushButton::clicked,              this,   [=]()           {   sumTable(mFacade->getSecondaryModel("AntiBio", 0));    }    );
//    connect(ui->btnSumTblSubStrats,             &QPushButton::clicked,              this,   [=]()           {   sumTable(mFacade->getSecondaryModel("Substrats", 0));    }    );
}


MainWindow::~MainWindow()
{
    delete ui;
    delete mFacade;
    delete antibioWindow;
    delete substratsWindow;
}



///////////////////////////////////////////////////////////////////////////////
void MainWindow::initFields()    {
    fields.append(QPair<QString, QString>("idStrain", "id"));
    fields.append(QPair<QString, QString>("Name", "Название"));
    fields.append(QPair<QString, QString>("ExtrPlace", "Место выделения"));
    fields.append(QPair<QString, QString>("ExtrPlaceDisc", "Описание места выделения"));
    fields.append(QPair<QString, QString>("ExtrDate", "Дата выделения"));
    fields.append(QPair<QString, QString>("ExtrEnv", "Среда выделения"));
    fields.append(QPair<QString, QString>("Temp", "Температура"));
    fields.append(QPair<QString, QString>("Taxonomy", "Таксономия"));
    fields.append(QPair<QString, QString>("GenBankDep", "Номер в ген. банке"));
    fields.append(QPair<QString, QString>("Class", "Группа"));
    fields.append(QPair<QString, QString>("Fridge_p4", "Хол. +4, кор.№"));
    fields.append(QPair<QString, QString>("Fridge_m70", "Хол. -70, п.№"));

    fields.append(QPair<QString, QString>("idColony", "id кол."));
    fields.append(QPair<QString, QString>("Form", "Форма"));
    fields.append(QPair<QString, QString>("Type", "Морфотип"));
    fields.append(QPair<QString, QString>("ColonySize", "Размер колонии"));
    fields.append(QPair<QString, QString>("CellSize", "Размер клетки"));
    fields.append(QPair<QString, QString>("SporeSize", "Размер спор"));
    fields.append(QPair<QString, QString>("Spore", "Спорообразование"));
    fields.append(QPair<QString, QString>("Color", "Цвет"));
    fields.append(QPair<QString, QString>("Shine", "Блеск"));
    fields.append(QPair<QString, QString>("Transparency", "Прозрачность"));
    fields.append(QPair<QString, QString>("Edge", "Края"));
    fields.append(QPair<QString, QString>("Surface", "Поверхность"));
    fields.append(QPair<QString, QString>("Profile", "Профиль"));
    fields.append(QPair<QString, QString>("Growth", "Рост по штриху"));
    fields.append(QPair<QString, QString>("Pigment", "Пигментообразование"));
    fields.append(QPair<QString, QString>("idStrain_Colony", "id штамма (кол.)"));

    fields.append(QPair<QString, QString>("idBioChem", "id биохим."));
    fields.append(QPair<QString, QString>("RelCasein", "Рост на казеине/активность"));
    fields.append(QPair<QString, QString>("RelCrachmal", "Рост на крахмале/активность"));
    fields.append(QPair<QString, QString>("RelTween", "Рост на твине/активность"));
    fields.append(QPair<QString, QString>("TempRange", "Диапазон температур/оптимум, С"));
    fields.append(QPair<QString, QString>("PhRange", "Диапазон рН/оптимум рН"));
    fields.append(QPair<QString, QString>("RelNaCl", "Отношение к NaCl, 1 г/5 л"));
    fields.append(QPair<QString, QString>("RelOxigen", "Отношение к кислороду"));
    fields.append(QPair<QString, QString>("Feeding", "Тип питания"));
    fields.append(QPair<QString, QString>("idStrain_BioChem", "id штамма (биохим.)"));

    fields.append(QPair<QString, QString>("idSeq", "id секв."));
    fields.append(QPair<QString, QString>("rrnkFile", "Секв. 16spPРК файл"));
    fields.append(QPair<QString, QString>("rrnkComment", "Секв. 16spPРК коммент."));
    fields.append(QPair<QString, QString>("seqFullFile", "Полногеном. секв. файл"));
    fields.append(QPair<QString, QString>("seqFullComment", "Полногеном. секв. коммент."));
    fields.append(QPair<QString, QString>("massSpectrFile", "Масс-спектр. файл"));
    fields.append(QPair<QString, QString>("massSpectrComment", "Масс-спектр. коммент."));
    fields.append(QPair<QString, QString>("idStrain_Seq", "id штамма (секв.)"));

}


void MainWindow::initMenu() {
    QMenu *dataMenu = menuBar()->addMenu("Данные");
    dataMenu->addAction(QObject::tr("Добавить штамм"),
                          [&]()     {   add();  }   );
    dataMenu->addAction(QObject::tr("Удалить штамм"),
                          [&]()     {   del();  }   );
    dataMenu->addSeparator();
    dataMenu->addAction(QObject::tr("Экспорт таблицы"),
                      [&]()     {   exportTable();    }   );
    dataMenu->addAction(QObject::tr("Экспорт выделенного"),
                      [&]()     {   exportTable(false);    }   );
    dataMenu->addSeparator();
    dataMenu->addAction(QObject::tr("Импорт базовых данных по штаммам"),
                      [&]()     {   mFacade->importBaseFromFile();  }   );
    dataMenu->addAction(QObject::tr("Импорт данных по колониям"),
                      [&]()     {   mFacade->importNotBaseFromFile("Colony", "idStrain_Colony");    }   );
    dataMenu->addAction(QObject::tr("Импорт данных по биохимии"),
                      [&]()     {   mFacade->importNotBaseFromFile("BioChem", "idStrain_BioChem");    }   );
    dataMenu->addAction(QObject::tr("Импорт данных по антибиотикам"),
                      [&]()     {   antibioWindow->importData();    }   );
    dataMenu->addAction(QObject::tr("Импорт данных по субстратам"),
                      [&]()     {   substratsWindow->importData();  }   );
    dataMenu->addSeparator();
    dataMenu->addAction(QObject::tr("Обновление базовых данных по штаммам"),
                      [&]()     {   mFacade->updateBaseFromFile();  }   );
    dataMenu->addAction(QObject::tr("Обновление данных по колониям"),
                      [&]()     {   mFacade->updateNotBaseFromFile("Colony", "idStrain_Colony");    }   );
    dataMenu->addAction(QObject::tr("Обновление данных по биохимии"),
                      [&]()     {   mFacade->updateNotBaseFromFile("BioChem", "idStrain_BioChem");    }   );
    dataMenu->addAction(QObject::tr("Обновление данных по антибиотикам"),
                      [&]()     {   antibioWindow->updateData();    }   );
    dataMenu->addAction(QObject::tr("Обновление данных по субстратам"),
                      [&]()     {   substratsWindow->updateData();  }   );
//    dataMenu->addSeparator();
//    dataMenu->addAction(QObject::tr("Add fake data"),
//                      [&]()     {   mFacade->addFake();     }   );
//    dataMenu->addAction(QObject::tr("Del fake data"),
//                      [&]()     {   mFacade->delFake();     }   );

    QMenu *dbMenu = menuBar()->addMenu("База данных");
    dbMenu->addAction(QObject::tr("Резервная копия"),
                      [&]()     {   pathesDB();     }   );
    dbMenu->addAction(QObject::tr("Соединение с БД"),
                      [&]()     {   connectDB();    }   );

    dbMenu->addSeparator();
    dbMenu->addAction(QObject::tr("Экспорт БД в CSV-файлы"),
                      [&]()     {   mFacade->backupAll();   }   );
    dbMenu->addAction(QObject::tr("Импорт БД из CSV-файлов"),
                      [&]()     {   mFacade->restoreAll();  }   );

    QMenu *classMenu = menuBar()->addMenu("Списки параметров");
    classMenu->addAction(QObject::tr("Столбцы таблицы"),
                      [&]()     {   setFields();  setActualFields();}   );
    classMenu->addSeparator();
    classMenu->addAction(QObject::tr("Группы штаммов"),
                         [&]()  {   setValuesList("Strain_Classes", "Item", "Группы штаммов"); }   );
    classMenu->addAction(QObject::tr("Морфотипы клеток"),
                         [&]()  {   setValuesList("Cell_Type", "Item", "Морфотипы клеток"); }   );
    classMenu->addAction(QObject::tr("Степени блеска колонии"),
                         [&]()  {   setValuesList("Colony_Glitter", "Item", "Степени блеска колонии"); }   );
    classMenu->addAction(QObject::tr("Степени прозрачности колонии"),
                         [&]()  {   setValuesList("Colony_Transparency", "Item", "Степени прозрачности колонии"); }   );
    classMenu->addAction(QObject::tr("Типы поверхности колонии"),
                         [&]()  {   setValuesList("Colony_Surface", "Item", "Типы поверхности колонии"); }   );
    classMenu->addAction(QObject::tr("Типы профиля колонии"),
                         [&]()  {   setValuesList("Colony_Profile", "Item", "Типы профиля колонии"); }   );
    classMenu->addAction(QObject::tr("Типы роста колонии по штриху"),
                         [&]()  {   setValuesList("Colony_Growth", "Item", "Типы роста колонии по штриху"); }   );
    classMenu->addAction(QObject::tr("Отношение к кислороду"),
                         [&]()  {   setValuesList("BioCh_Oxigen", "Item", "Отношение к кислороду"); }   );
    classMenu->addAction(QObject::tr("Типы питания"),
                         [&]()  {   setValuesList("BioCh_Feeding", "Item", "Типы питания"); }   );
//    classMenu->addAction(QObject::tr("Универсальный параметр"),
//                         [&]()  {   setValuesList("UniParam", "Item", "Универсальный параметр"); }   );

    QMenu *secMenu = menuBar()->addMenu("Доп. данные");
    secMenu->addAction(QObject::tr("Антибиотики. Общие данные"),
                      [&]()     {   antibioWindow->show();      }   );
    secMenu->addAction(QObject::tr("Антибиотики. Сводные данные"),
                      [&]()     {   sumTable(mFacade->getSecondaryModel("AntiBio", 0));     }   );
    secMenu->addSeparator();
    secMenu->addAction(QObject::tr("Субстраты. Общие данные"),
                      [&]()     {   substratsWindow->show();    }   );
    secMenu->addAction(QObject::tr("Субстраты. Сводные данные"),
                      [&]()     {   sumTable(mFacade->getSecondaryModel("Substrats", 0));   }   );

    QMenu *groupMenu = menuBar()->addMenu("Групповое редактирование");
    groupMenu->addAction(QObject::tr("По выделенным строкам"),
                      [&]()     {   groupEditSel();      }   );
    groupMenu->addAction(QObject::tr("По группам штаммов"),
                      [&]()     {   groupEditParam();      }   );

}


//void MainWindow::initToolBar()  {
//    ui->mainToolBar->addAction("Антибиотики",                           [=]()     {   antibioWindow->show();    });
//    ui->mainToolBar->addAction("Св. данные антибиотиков",               [=]()     {   sumTable(mFacade->getSecondaryModel("AntiBio", 0));       }    );
//    ui->mainToolBar->addSeparator();
//    ui->mainToolBar->addAction("Субстраты",                             [=]()     {   substratsWindow->show();  });
//    ui->mainToolBar->addAction("Св. данные субстратов",                 [=]()     {   sumTable(mFacade->getSecondaryModel("Substrats", 0));     }    );
//    ui->mainToolBar->addSeparator();
//    ui->mainToolBar->addAction("Групп. редактирование (выделенное)",    [=]()     {   groupEditSel();     }    );
//}


void MainWindow::initSettings() {
    QSettings settings("settings.ini", QSettings::IniFormat);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    settings.setIniCodec(codec);

    if (!settings.contains("DatabaseConnection/Driver"))           settings.setValue("DatabaseConnection/Driver", "QMYSQL");
    if (!settings.contains("DatabaseConnection/ConnectionName"))   settings.setValue("DatabaseConnection/ConnectionName", "stepic");
    if (!settings.contains("DatabaseConnection/UserName"))         settings.setValue("DatabaseConnection/UserName", "root");
    if (!settings.contains("DatabaseConnection/Password"))         settings.setValue("DatabaseConnection/Password", "11111111");
    if (!settings.contains("DatabaseConnection/DatabaseName"))     settings.setValue("DatabaseConnection/DatabaseName", "ICG");
    if (!settings.contains("DatabaseConnection/HostName"))         settings.setValue("DatabaseConnection/HostName", "localhost");
    if (!settings.contains("DatabaseConnection/Port"))             settings.setValue("DatabaseConnection/Port", 3306);

    if (!settings.contains("Path/BackupPath"))                     settings.setValue("Path/BackupPath",    QApplication::applicationDirPath() + "/backup");
//    if (!settings.contains("Path/ColonyDB"))                       settings.setValue("Path/ColonyDB",       "D:/Projects/Qt/ICG_base/ICG_ColonyBase.csv");
//    if (!settings.contains("Path/BioChemDB"))                      settings.setValue("Path/BioChemDB",      "D:/Projects/Qt/ICG_base/ICG_BioChemBase.csv");
//    if (!settings.contains("Path/SeqDB"))                          settings.setValue("Path/SeqDB",          "D:/Projects/Qt/ICG_base/ICG_SeqBase.csv");
//    if (!settings.contains("Path/AntiBioDB"))                      settings.setValue("Path/AntiBioDB",      "D:/Projects/Qt/ICG_base/ICG_AntiBioBase.csv");
//    if (!settings.contains("Path/SubstratsDB"))                    settings.setValue("Path/SubstratsDB",    "D:/Projects/Qt/ICG_base/ICG_SubstratsBase.csv");

    if (!settings.contains("Fields/TableFields") || (settings.value("Fields/TableFields").toString().length() < fields.size()))
                                                                   settings.setValue("Fields/TableFields", QString::number(0).rightJustified(fields.size(), '0'));

    if (!settings.contains("Strain_Classes/Item_0"))               settings.setValue("Strain_Classes/Item_0", QString::fromUtf8("Протеолитики"));
    if (!settings.contains("Cell_Type/Item_0"))                    settings.setValue("Cell_Type/Item_0", QString::fromUtf8("Палочки"));
    if (!settings.contains("Colony_Glitter/Item_0"))               settings.setValue("Colony_Glitter/Item_0", QString::fromUtf8("Блестящая"));
    if (!settings.contains("Colony_Transparency/Item_0"))          settings.setValue("Colony_Transparency/Item_0", QString::fromUtf8("Непрозрачная"));
    if (!settings.contains("Colony_Surface/Item_0"))               settings.setValue("Colony_Surface/Item_0", QString::fromUtf8("Гладкая"));
    if (!settings.contains("Colony_Profile/Item_0"))               settings.setValue("Colony_Profile/Item_0", QString::fromUtf8("Изогнутый"));
    if (!settings.contains("Colony_Growth/Item_0"))                settings.setValue("Colony_Growth/Item_0", QString::fromUtf8("Четкой видный"));
    if (!settings.contains("BioCh_Oxigen/Item_0"))                 settings.setValue("BioCh_Oxigen/Item_0", QString::fromUtf8("Аэроб"));
    if (!settings.contains("BioCh_Feeding/Item_0"))                settings.setValue("BioCh_Feeding/Item_0", QString::fromUtf8("Гетеротроф"));
    if (!settings.contains("UniParam/Item_0"))                     settings.setValue("UniParam/Item_0", QString::fromUtf8("+"));
    if (!settings.contains("UniParam/Item_1"))                     settings.setValue("UniParam/Item_1", QString::fromUtf8("+-"));
    if (!settings.contains("UniParam/Item_2"))                     settings.setValue("UniParam/Item_2", QString::fromUtf8("-+"));
    if (!settings.contains("UniParam/Item_3"))                     settings.setValue("UniParam/Item_3", QString::fromUtf8("-"));
    if (!settings.contains("UniParam/Item_4"))                     settings.setValue("UniParam/Item_4", QString::fromUtf8("Н.о."));
}


void MainWindow::setTableView()   {
    ui->tableView->setSortingEnabled(true);
    ui->tableView->horizontalHeader()->setSortIndicator(0, Qt::AscendingOrder);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
    //ui->tableView->setSelectionMode(QAbstractItemView::MultiSelection);
}


void MainWindow::setActiveElements()    {
    ui->btnSrch->setEnabled(db.isOpen());
    ui->lineSrch->setEnabled(db.isOpen());
    ui->cBoxSrch->setEnabled(db.isOpen());
    ui->tableView->setEnabled(db.isOpen());
    ui->menuBar;
    QList <QAction *> MenuList = ui->menuBar->actions();
    foreach (QAction *Menu, MenuList)  {
        if (Menu->text() != "База данных")
            Menu->setEnabled(db.isOpen());
    }
}



///////////////////////////////////////////////////////////////////////////////
void MainWindow::search()   {
    if (ui->lineSrch->text().isEmpty()) return;

    int idx = 0;
    while (fields[idx].second != ui->cBoxSrch->currentText())   {   idx++;  }

    mFacade->search(ui->lineSrch->text(), fields[idx].first);
    setTableView();
    setActualFields();
}


void MainWindow::add() {
    QVector<QStringList> vecValues;
    QStringList list;
    for (int i = 0; i < 4; i++)
        vecValues.push_back(list);

    BartProxyModel *m_antiBio = mFacade->getSecondaryModel("AntiBio", 0);
    BartProxyModel *m_substrats = mFacade->getSecondaryModel("Substrats", 0);
    StrainSheetForm *sheet = new StrainSheetForm(vecValues, m_antiBio, m_substrats);
    sheet->show();

    connect(sheet,  &StrainSheetForm::strainDataAdded,      this,   &MainWindow::strainSheetNewItem     );
    connect(sheet,  &StrainSheetForm::strainNewItem,        this,   &MainWindow::strainSheetNewItemID   );
    connect(sheet,  &StrainSheetForm::strainDataChanged,    this,   &MainWindow::strainSheetDataEdited  );
}


void MainWindow::del()  {
    QModelIndexList list = ui->tableView->selectionModel()->selectedRows();
    if (list.isEmpty())
        return;

    mFacade->delItem(list);
//    for (int i = 0; i < list.count(); i++)
//        mFacade->delItem(list[i].row());
}


void MainWindow::view(const QModelIndex &index) {
    QVector<QStringList> vecValues = mFacade->rowValueVector(index);
    int idx;
    if (!vecValues[0].isEmpty())
        idx = static_cast<QString>(vecValues[0][0]).toInt();
    BartProxyModel *m_antiBio = mFacade->getSecondaryModel("AntiBio", idx);
    BartProxyModel *m_substrats = mFacade->getSecondaryModel("Substrats", idx);
    StrainSheetForm *sheet = new StrainSheetForm(vecValues, m_antiBio, m_substrats);
    sheet->show();

    connect(sheet,  &StrainSheetForm::strainDataAdded,      this,   &MainWindow::strainSheetNewItem     );
    connect(sheet,  &StrainSheetForm::strainNewItem,        this,   &MainWindow::strainSheetNewItemID   );
    connect(sheet,  &StrainSheetForm::strainDataChanged,    this,   &MainWindow::strainSheetDataEdited  );
}



///////////////////////////////////////////////////////////////////////////////
void MainWindow::strainSheetNewItemID(StrainSheetForm * sheet, QString tableName) {
    sheet->setID(tableName, mFacade->lastID(tableName));
}


void MainWindow::strainSheetNewItem(QStringList &listValues, QString tableName)  {
    mFacade->addItem(tableName, listValues);
    setTableView();
}


void MainWindow::strainSheetDataEdited(QStringList listValues, QString tableID, QString strainID, QString tableName)  {
    mFacade->editItem(tableName, tableID.toInt(), listValues);
    setTableView();
}



///////////////////////////////////////////////////////////////////////////////
void MainWindow::connectDB()
{
    SqlConnectionDialog dialog(this);
    if (dialog.exec() != QDialog::Accepted)
        return;

    QSettings settings("settings.ini", QSettings::IniFormat);

    if (db.isOpen()) db.close();
    //db.removeDatabase(settings.value("DatabaseConnection/ConnectionName").toString());

//    settings.setValue("DatabaseConnection/Driver", dialog.driverName());
//    settings.setValue("DatabaseConnection/ConnectionName", dialog.connectionName());
    settings.setValue("DatabaseConnection/UserName", dialog.userName());
    settings.setValue("DatabaseConnection/Password", dialog.password());
    settings.setValue("DatabaseConnection/DatabaseName", dialog.databaseName());
    settings.setValue("DatabaseConnection/HostName", dialog.hostName());
    settings.setValue("DatabaseConnection/Port", dialog.port());

    openDB();
    setActiveElements();
    setTableView();
    setActualFields();
}


void MainWindow::openDB()
{
    QSettings settings("settings.ini", QSettings::IniFormat);

    db.setUserName(settings.value("DatabaseConnection/UserName").toString());
    db.setPassword(settings.value("DatabaseConnection/Password").toString());
    db.setHostName(settings.value("DatabaseConnection/HostName").toString());
    db.setPort(settings.value("DatabaseConnection/Port", 3306).toInt());
    db.setDatabaseName(settings.value("DatabaseConnection/DatabaseName").toString());
    if (db.open()) {
        ui->statusBar->showMessage("Соединение с базой данных установлено");
    } else {
        ui->statusBar->showMessage("Соединение с базой данных НЕ УДАЛОСЬ");
    }
}


void MainWindow::pathesDB()    {
    DBPathesDialog dialog(this);
    if (dialog.exec() != QDialog::Accepted)
        return;

    QSettings settings("settings.ini", QSettings::IniFormat);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    settings.setIniCodec(codec);

    settings.setValue("Path/BackupPath", dialog.strainPath());
//    settings.setValue("Path/ColonyDB", dialog.colonyPath());
//    settings.setValue("Path/BioChemDB", dialog.bioChemPath());
//    settings.setValue("Path/SeqDB", dialog.seqPath());
//    settings.setValue("Path/AntiBioDB", dialog.antiBioPath());
//    settings.setValue("Path/SubstratsDB", dialog.substratsPath());

    mFacade->setPathStrain(settings.value("Path/BackupPath").toString());
//    mFacade->setPathColony(settings.value("Path/ColonyDB").toString());
//    mFacade->setPathBioChem(settings.value("Path/BioChemDB").toString());
//    mFacade->setPathSeq(settings.value("Path/SeqDB").toString());
//    mFacade->setPathAntiBio(settings.value("Path/AntiBioDB").toString());
//    mFacade->setPathSubstrats(settings.value("Path/SubstratsDB").toString());
}



///////////////////////////////////////////////////////////////////////////////
void MainWindow::setFields()    {
    QStringList list;
    foreach (auto x, fields)
        list << x.second;

    TableFieldsDialog dialog(list, this);
    if (dialog.exec() != QDialog::Accepted)
        return;

    list.erase(list.begin(), list.end());
    list = dialog.fields();

    QString str = "";
    QList<QPair<QString, QString>> actualFields;

    foreach (auto x, fields) {
        if (list.contains(x.second))    {
            str += '1';
            actualFields.append(x);
        } else str += '0';
    }

    QSettings settings("settings.ini", QSettings::IniFormat);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    settings.setIniCodec(codec);
    settings.setValue("Fields/TableFields", str);
    //mFacade->setFields(actualFields);
    //setActualFields();
    setTableView();
}


void MainWindow::setActualFields()    {
    QSettings settings("settings.ini", QSettings::IniFormat);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    settings.setIniCodec(codec);

    for (int i = 0; i < settings.value("Fields/TableFields").toString().length(); i++)  {
        if (ui->tableView->model()->headerData(i, Qt::Horizontal) == fields[i].first)
            ui->tableView->model()->setHeaderData(i, Qt::Horizontal, fields[i].second);
        if (settings.value("Fields/TableFields").toString().at(i) == '1')
            ui->tableView->showColumn(i);
        else
            ui->tableView->hideColumn(i);
    }
}


void MainWindow::setValuesList(QString groupName, QString itemName, QString dialogName) {
    ValuesListDialog dialog(groupName, itemName, dialogName, this);
    if (dialog.exec() != QDialog::Accepted)
        return;

    QStringList list = dialog.value_list();
    QSettings settings("settings.ini", QSettings::IniFormat);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    settings.setIniCodec(codec);

    for (int i = 0; i < list.count(); i++)  {
        QString str = list[i];
        settings.setValue(QString("%1/%2_%3").arg(groupName).arg(itemName).arg(i), str);
    }

    for (int i = list.count(); settings.value(QString("%1/%2_%3").arg(groupName).arg(itemName).arg(i)).isValid(); i++)  {
        settings.remove(QString("%1/%2_%3").arg(groupName).arg(itemName).arg(i));
    }
}


void MainWindow::header_clicked(int logicalIndex) {
    mFacade->sort(fields[logicalIndex].first, ui->tableView->horizontalHeader()->sortIndicatorOrder());
}


void MainWindow::keyPressEvent(QKeyEvent *event)
{
    int key = event->key();
    if ( (key == Qt::Key_Return || key == Qt::Key_Enter) &&
         this->ui->cBoxSrch->hasFocus() )
    {
        MainWindow::search();
    } else
        QMainWindow::keyPressEvent(event);
}



///////////////////////////////////////////////////////////////////////////////
void MainWindow::exportTable(bool allTable)   {
    if (!allTable && ui->tableView->selectionModel()->selectedRows().isEmpty()) {
        QMessageBox::information(0,
                                 tr("Экспорт данных"),
                                 tr("Нет выделенных записей"));
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this,
                                   tr("Export Data"),
                                   QCoreApplication::applicationDirPath(),
                                   tr ("Microsoft Excel (*.xls);; HTML (*.html);; PDF (*.pdf)"));
    if (!fileName.isEmpty())    {
        QString fileFormat = QStringList(fileName.split('.')).last();
        if (fileFormat == "xls")
            exportXLS(fileName, allTable);
        else if (fileFormat == "html")
            exportHTML(fileName, allTable);
        else if (fileFormat == "pdf")
            exportPDF(fileName, allTable);
    }

    int rowCnt = allTable ? ui->tableView->model()->rowCount() : ui->tableView->selectionModel()->selectedRows().count();
    QMessageBox::information(0,
                             tr("Экспорт данных"),
                             tr("Строк экспортировано: ") + QString::number(rowCnt));
}


void MainWindow::exportXLS(QString fileName, bool allTable)
{
    QModelIndexList list = ui->tableView->selectionModel()->selectedRows();

    QAxObject *excel = new QAxObject("Excel.Application",this);
    excel->setProperty("Visible", false);                                            //видимость
    QAxObject *workbooks = excel->querySubObject("Workbooks");
    QAxObject *workbook = workbooks->querySubObject("Add()");
    QAxObject *sheets = workbook->querySubObject("Sheets");
    QAxObject *StatSheet = sheets->querySubObject("Item(1)");
    StatSheet->dynamicCall("Select()");

    QString header, value;
    int cols = ui->tableView->horizontalHeader()->count();
    int rows = allTable ?
                ui->tableView->verticalHeader()->count() : list.count();
    for(int i = 0; i < cols; i++)        {
        header = ui->tableView->model()->headerData(i, Qt::Horizontal).toString();
        QAxObject *cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", 1, i+1);                    // Row #1, start from col #1
        cell->setProperty("Value", header);
        QAxObject *font = cell->querySubObject("Font");
        font->setProperty("Bold", true);
        delete font;
        delete cell;
    }

    for(int i = 0; i < rows; i++)   {
        for(int j = 0; j < cols; j++)   {
            QModelIndex idx = allTable ?
                        ui->tableView->model()->index(i, j) : ui->tableView->model()->index(list[i].row(), j);
            value = ui->tableView->model()->data(idx).toString();
            QAxObject *cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", i+2, j+1);                  // Start from row #2, col #1
            cell->setProperty("Value", value);
            value="";
            QAxObject *prop = cell->querySubObject("EntireColumn");
            prop->dynamicCall("AutoFit()");
            delete prop;
            delete cell;
        }
    }
    // memory clearing
    delete StatSheet;
    delete sheets;
    //workbook->dynamicCall("Save()");
    excel->setProperty("DisplayAlerts", 0);
    fileName.replace('/', "\\");
    QList<QVariant> lstParam;
    lstParam.append(fileName);
    lstParam.append(-4143);
    lstParam.append("");
    lstParam.append("");
    lstParam.append(false);
    lstParam.append(false);
    lstParam.append(1);
    lstParam.append(2);
    lstParam.append(false);
    lstParam.append(false);
    lstParam.append(false);
    lstParam.append(false);

    workbook->dynamicCall("SaveAs(QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant)", lstParam);
    workbook->dynamicCall("Close (Boolean)", false);
    delete workbook;
    delete workbooks;
    excel->setProperty("DisplayAlerts", 1);
    excel->dynamicCall("Quit(void)");
    delete excel;
}


void MainWindow::exportHTML(QString fileName, bool allTable)
{
    QFile file(fileName);
    if(file.open(QIODevice::WriteOnly)) {
        QTextStream stream(&file);
        stream << getHTMLtoExport(allTable);
    }
    file.close();
}


void MainWindow::exportPDF(QString fileName, bool allTable)
{
    QTextDocument document;
    QPrinter printer (QPrinter::HighResolution);
    QString textHtml = getHTMLtoExport(allTable);

    document.setHtml(textHtml);
    printer.setOutputFormat( QPrinter::PdfFormat );
    printer.setOrientation(QPrinter::Landscape);
    printer.setOutputFileName( fileName );
    document.print( &printer );
}


QString MainWindow::getHTMLtoExport(bool allTable)  {
    QModelIndexList list = ui->tableView->selectionModel()->selectedRows();
    QString textHtml;
    int cols = ui->tableView->horizontalHeader()->count();
    int rows = allTable ?
                ui->tableView->verticalHeader()->count() : list.count();
        {
        textHtml += "<table border=\"1\" width=\"100%\">";
        textHtml += "<tr>";
        for (int i = 0; i < cols; i++)
            textHtml += "<td><p align=\"center\"><b>" + ui->tableView->model()->headerData(i, Qt::Horizontal).toString() + "</b></p></td>";
        textHtml += "</tr>";
        for (int i = 0; i < rows; i++)  {
            textHtml += "<tr>";
            for (int j = 0; j < cols; j++)  {
                QModelIndex idx = allTable ?
                            ui->tableView->model()->index(i, j) : ui->tableView->model()->index(list[i].row(), j);
                textHtml += "<td><p align=\"center\">" + ui->tableView->model()->data(idx).toString() + "</p></td>";
            }
            textHtml += "</tr>";
        }
        textHtml += "</table>";
    }

    return textHtml;
}


//void MainWindow::exportODT(QString fileName)
//{
//    QTextDocument document;
//    QString textHtml;

//    textHtml += "<table border=\"2\" width=\"100%\">";
//    textHtml += "<tr>";
//    for (int i = 0; i < ui->tableView->horizontalHeader()->count(); i++)
//        textHtml += "<td><p align=\"center\"><b>" + ui->tableView->model()->headerData(i, Qt::Horizontal).toString() + "</b></p></td>";
//        textHtml += "</tr>";
//    for (int i = 0; i < ui->tableView->verticalHeader()->count(); i++)  {
//        textHtml += "<tr>";
//        for (int j = 0; j < ui->tableView->horizontalHeader()->count(); j++)
//            textHtml += "<td><p align=\"center\">" + ui->tableView->model()->data(ui->tableView->model()->index(i, j)).toString() + "</p></td>";
//        textHtml += "</tr>";
//    }
//    textHtml += "</table>";

//    document.setHtml(textHtml);

//    QTextDocumentWriter writer(fileName);
//    writer.setFormat("odf");
//    writer.write(&document);
//}



///////////////////////////////////////////////////////////////////////////////
void MainWindow::sumTable(BartProxyModel *secModel) {
    QModelIndexList list = ui->tableView->selectionModel()->selectedRows();
    if (list.isEmpty()) {
        QMessageBox::information(this,
                                   tr("Сводная таблица"),
                                   tr("Штаммы не выбраны"));
        return;
    }

    QAxObject *excel = new QAxObject("Excel.Application",this);
    QAxObject *workbooks = excel->querySubObject("Workbooks");
    QAxObject *workbook = workbooks->querySubObject("Add()");
    QAxObject *sheets = workbook->querySubObject("Sheets");
    QAxObject *StatSheet = sheets->querySubObject("Item(1)");
    StatSheet->dynamicCall("Select()");

    QString filterParam, filterValue, headerStrain, headerObj, valueObj;
    filterParam = secModel->record().fieldName(4);

    QAxObject *cell, *prop, *font;

    for (int i = 0; i < list.count(); i++) {
        headerStrain = ui->tableView->model()->data(ui->tableView->model()->index(list[i].row(), 1)).toString();

        cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", i+2, 1);                  // Start from row #2, col #1
        cell->setProperty("Value", headerStrain);
        prop = cell->querySubObject("EntireColumn");
        prop->dynamicCall("AutoFit()");
        font = cell->querySubObject("Font");
        font->setProperty("Bold", true);
        delete font;
        delete prop;
        delete cell;

        filterValue = ui->tableView->model()->data(ui->tableView->model()->index(list[i].row(), 0)).toString();
        secModel->setFilter(QString("%1 = %2").arg(filterParam, filterValue));
        for (int j = 0; j < secModel->rowCount(); j++)  {
            int k = 2;
            headerObj = secModel->data(secModel->index(j, 1)).toString();
            valueObj = secModel->data(secModel->index(j, 2)).toString();

            while (!StatSheet->querySubObject("Cells(QVariant,QVariant)", 1, k)->property("Value").isNull() &&
                  (StatSheet->querySubObject("Cells(QVariant,QVariant)", 1, k)->property("Value").toString() != headerObj))   k++;
            cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", 1, k);
            cell->setProperty("Value", headerObj);
            prop = cell->querySubObject("EntireColumn");
            prop->dynamicCall("AutoFit()");
            font = cell->querySubObject("Font");
            font->setProperty("Bold", true);
            delete font;
            delete prop;
            delete cell;

            cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", i+2, k);
            cell->setProperty("Value", valueObj);
            prop = cell->querySubObject("EntireColumn");
            prop->dynamicCall("AutoFit()");
            delete prop;
            delete cell;
        }
    }
    excel->setProperty("Visible", true);
    QMessageBox::StandardButton reply;
    reply = QMessageBox::information(this,
                           tr("Сводная таблица"),
                           tr("Сводная таблица готова"));

    secModel->setFilter(QString("%1 = ''").arg(filterParam));
    delete StatSheet;
    delete sheets;
    delete workbook;
    delete workbooks;
    delete excel;
}



///////////////////////////////////////////////////////////////////////////////
void MainWindow::groupEditParam()   {
    GroupEditParamDialog dialog(this);
    if (dialog.exec() != QDialog::Accepted)
        return;

    QStringList listID = mFacade->searchID(dialog.fields(), "Class");
    if (listID.isEmpty()) {
        QMessageBox::information(this, tr("Групповое редактирование"), tr("Группы не найдены"));
        return;
    }
    groupView(listID);
}


void MainWindow::groupEditSel()    {
    QModelIndexList list = ui->tableView->selectionModel()->selectedRows();
    if (list.isEmpty()) {
        QMessageBox::information(this, tr("Групповое редактирование"), tr("Штаммы не выбраны"));
        return;
    }

    QStringList listID;
    for (int i = 0; i < list.count(); i++) {
        listID << list.at(i).data().toString();
    }
    groupView(listID);
}


void MainWindow::groupView(QStringList listID)  {
    QVector<QStringList> vecValues, vecValuesNext;
    vecValues = mFacade->rowValueVector(static_cast<QString>(listID.at(0)).toInt());
    for (int i = 1; i < listID.count(); i++) {
        vecValuesNext = mFacade->rowValueVector(static_cast<QString>(listID.at(i)).toInt());
        for(int m = 0; m < vecValuesNext.count(); m++)   {
            if (!vecValuesNext[m].isEmpty())    {
                if (vecValues[m].isEmpty())
                    for (int n = 0; n < vecValuesNext[m].count(); n++)
                        vecValues[m] << "[Значения не совпадают]";
                else {
                    for (int n = 0; n < vecValuesNext[m].count(); n++)  {
                        if (vecValues[m][n] != vecValuesNext[m][n])
                            vecValues[m][n] = "[Значения не совпадают]";
                    }
                }
            } else {
                if (!vecValues[m].isEmpty())
                    for (int n = 0; n < vecValues[m].count(); n++)
                        vecValues[m][n] = "[Значения не совпадают]";
            }
        }
    }
    vecValues[0][0] = listID.join("/");

    BartProxyModel *m_antiBio = mFacade->getSecondaryModel("AntiBio", 0);
    BartProxyModel *m_substrats = mFacade->getSecondaryModel("Substrats", 0);
    StrainSheetForm *sheet = new StrainSheetForm(vecValues, m_antiBio, m_substrats);
    sheet->show();

    connect(sheet,  &StrainSheetForm::strainDataAdded,      this,   &MainWindow::strainSheetNewGroupItem        );
    connect(sheet,  &StrainSheetForm::strainDataChanged,    this,   &MainWindow::strainSheetGroupDataEdited     );
}


///////////////////////////////////////////////////////////////////////////////
void MainWindow::strainSheetNewGroupItem(QStringList &listValuesGroup, QString tableName)  {
    QStringList listID = static_cast<QString>(listValuesGroup.at(listValuesGroup.count()-1)).split("/");

    for (int i = 0; i < listID.count(); i++)    {
        listValuesGroup[listValuesGroup.count()-1] = listID[i];
        mFacade->addItem(tableName, listValuesGroup);
    }
    setTableView();
}


void MainWindow::strainSheetGroupDataEdited(QStringList listValuesGroup, QString tableID, QString strainID, QString tableName)  {
    int tableIdxOld;
    if (tableName == "Strain")
        tableIdxOld = 0;
    else if (tableName == "Colony")
        tableIdxOld = 1;
    else if (tableName == "BioChem")
        tableIdxOld = 2;
    else if (tableName == "Seq")
        tableIdxOld = 3;

    QStringList listID = strainID.split("/");
    QStringList listValuesOld, listValuesNew;
    for (int i = 0; i < listID.count(); i++)    {
        QString unitID = listID[i];
        listValuesOld = mFacade->rowValueVector(unitID.toInt()).at(tableIdxOld);
        if (!listValuesOld.isEmpty())   {
            for (int j = 0; j < listValuesGroup.count(); j++)   {
                if (listValuesGroup.at(j) == "[Значения не совпадают]")
                    listValuesNew << listValuesOld.at(j+1);
                else listValuesNew << listValuesGroup.at(j);
            }
            //qDebug() << listValuesNew;
            mFacade->editItem(tableName, static_cast<QString>(listValuesOld.at(0)).toInt(), listValuesNew);
        } else {
            for (int j = 0; j < listValuesGroup.count(); j++)   {
                if (listValuesGroup.at(j) == "[Значения не совпадают]")
                    listValuesNew << "";
                else listValuesNew << listValuesGroup.at(j);
            }
            listValuesNew[listValuesGroup.count()-1] = unitID;
            mFacade->addItem(tableName, listValuesNew);
        }
        listValuesNew.erase(listValuesNew.begin(), listValuesNew.end());
    }
    setTableView();
}
