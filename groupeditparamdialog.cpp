#include "groupeditparamdialog.h"
#include "ui_groupeditparamdialog.h"

GroupEditParamDialog::GroupEditParamDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GroupEditParamDialog)
{
    ui->setupUi(this);
    setWindowTitle("Выбор групп");

    mainLayout = new QVBoxLayout;
    chbLayout = new QVBoxLayout;
    btnLayout = new QHBoxLayout;
    btnOK = new QPushButton("OK");
    btnCancel = new QPushButton("Отмена");
    btnLayout->addWidget(btnOK);
    btnLayout->addWidget(btnCancel);

    QSettings settings("settings.ini", QSettings::IniFormat);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    settings.setIniCodec(codec);

    for (int i = 0; settings.value(QString("Strain_Classes/Item_%1").arg(i)).isValid(); i++) {
        chbLayout->addWidget(new QCheckBox(settings.value(QString("Strain_Classes/Item_%1").arg(i)).toString()));
    }

    mainLayout->addLayout(chbLayout);
    mainLayout->addLayout(btnLayout);
    setLayout(mainLayout);

    connect(btnOK,      &QPushButton::clicked,      this,       &GroupEditParamDialog::accept);
    connect(btnCancel,  &QPushButton::clicked,      this,       &GroupEditParamDialog::reject);
}


GroupEditParamDialog::~GroupEditParamDialog()
{
    delete btnCancel;
    delete btnOK;
    delete btnLayout;
    delete chbLayout;
    delete mainLayout;
    delete ui;
}



QString GroupEditParamDialog::fields()  {
    QStringList list;
        for (int i = 0; i < chbLayout->count(); i++)       {
            if (static_cast<QCheckBox *>(chbLayout->itemAt(i)->widget())->isChecked())
                list << static_cast<QCheckBox *>(chbLayout->itemAt(i)->widget())->text();
        }

    return list.join(" / ");
}
