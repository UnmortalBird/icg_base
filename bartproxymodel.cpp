#include "bartproxymodel.h"


BartProxyModel::BartProxyModel()
{}


BartProxyModel::BartProxyModel(QObject *parent, QSqlDatabase db) :
    QSqlTableModel(parent, db)
{}


////////////////////////////////////////////////////////////////////////////////////////
QVariant BartProxyModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::FontRole)   {
        if (this->headerData(index.row(), Qt::Vertical).toString() == "*" ||
                this->headerData(index.row(), Qt::Vertical).toString() == "!")    {
            QFont font;
            font.setBold(true);
            return font;
        }
    }

    if (role == Qt::ForegroundRole) {
        if (this->headerData(index.row(), Qt::Vertical).toString() == "*")
        {
            return QBrush(Qt::blue);
        }
        if (this->headerData(index.row(), Qt::Vertical).toString() == "!")
        {
            return QBrush(Qt::red);
        }
    }
    return QSqlTableModel::data(index, role);
}


////////////////////////////////////////////////////////////////////////////////////////
int BartProxyModel::findDatainRow(QString Name) {
    for (int i = 0; i < this->rowCount(); i++) {
        if (this->data(this->index(i, 1)).toString() == Name)
            return i;
    }
    return -1;
}


int BartProxyModel::findDuplicate(QString Name, int foreignID)  {
    for (int i = 0; i < this->rowCount(); i++) {
        if ((this->data(this->index(i, 1)).toString() == Name) && (this->data(this->index(i,4)).toInt() == foreignID))
            return i;
    }
    return -1;
}

////////////////////////////////////////////////////////////////////////////////////////
void BartProxyModel::exportToFile()
{
    QString fileName = QFileDialog::getSaveFileName(0,
                                                    tr("Экспорт данных"),
                                                    QCoreApplication::applicationDirPath(),
                                                    tr ("Microsoft Excel (*.xls)"));
    if (!fileName.isEmpty())    {
        QAxObject *excel = new QAxObject("Excel.Application",this);
        excel->setProperty("Visible", false);
        QAxObject *workbooks = excel->querySubObject("Workbooks");
        QAxObject *workbook = workbooks->querySubObject("Add()");
        QAxObject *sheets = workbook->querySubObject("Sheets");
        QAxObject *StatSheet = sheets->querySubObject("Item(1)");
        StatSheet->dynamicCall("Select()");

        QString header, value;
        int cols = this->columnCount();
        int rows = this->rowCount();
        for(int i = 1; i < cols; i++)        {
            header = this->headerData(i, Qt::Horizontal).toString();
            QAxObject *cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", 1, i);                    // Row #1, start from col #1
            cell->setProperty("Value", header);
            QAxObject *font = cell->querySubObject("Font");
            font->setProperty("Bold", true);
            delete font;
            delete cell;
        }

        for(int i = 0; i < rows; i++)   {
            for(int j = 1; j < cols; j++)   {
                value = this->data(this->index(i, j)).toString();
                QAxObject *cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", i+2, j);                  // Start from row #2, col #1
                cell->setProperty("Value", value);
                value="";
                QAxObject *prop = cell->querySubObject("EntireColumn");
                prop->dynamicCall("AutoFit()");
                delete prop;
                delete cell;
            }
        }
        // memory clearing
        delete StatSheet;
        delete sheets;
        //workbook->dynamicCall("Save()");
        excel->setProperty("DisplayAlerts", 0);
        fileName.replace('/', "\\");
        QList<QVariant> lstParam;
        lstParam.append(fileName);
        lstParam.append(-4143);
        lstParam.append("");
        lstParam.append("");
        lstParam.append(false);
        lstParam.append(false);
        lstParam.append(1);
        lstParam.append(2);
        lstParam.append(false);
        lstParam.append(false);
        lstParam.append(false);
        lstParam.append(false);

        workbook->dynamicCall("SaveAs(QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant)", lstParam);
        workbook->dynamicCall("Close (Boolean)", false);
        delete workbook;
        delete workbooks;
        excel->setProperty("DisplayAlerts", 1);
        excel->dynamicCall("Quit(void)");
        delete excel;

        QMessageBox::information(0,
                                 tr("Экспорт данных"),
                                 tr("Строк экспортировано: ") + QString::number(this->rowCount()));
    }
}


void BartProxyModel::importFromFile()   {
    QString fileName = QFileDialog::getOpenFileName(0,
                                                    tr("Импорт данных"),
                                                    QCoreApplication::applicationDirPath(),
                                                    tr ("Microsoft Excel (*.xls)"));
    if (!fileName.isEmpty())    {
        QString filter = this->filter();
        this->setFilter("");
//        this->select();

        int cntNewRow = 0;
        QAxObject * excel = new QAxObject("Excel.Application",this);
        QAxObject * workbooks = excel->querySubObject("Workbooks");
        QAxObject * workbook = workbooks->querySubObject("Open(const QString&)", fileName);
        QAxObject * sheets = workbook->querySubObject("Sheets");
        QAxObject * StatSheet = sheets->querySubObject("Item(1)");
        StatSheet->dynamicCall("Select()");

        QSqlRecord record = this->record();
        record.remove(0);

        for (int row = 1; !StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").isNull(); row++)   {
//            qDebug() << StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 4)->property("Value").toInt();
//            qDebug() << this->data(this->index(0,4)).toInt();
//            bool checkStrain = (StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 4)->property("Value").toString() == this->data(this->index(0,4)).toString());
//            int checkName = this->findDatainRow(StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").toString());

            int checkRow = this->findDuplicate(StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").toString(),
                                               StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 4)->property("Value").toInt());
            if (checkRow > -1)   {
                QMessageBox::information(0,
                                         tr("Импорт данных"),
                                         tr("Такие данные уже присутствуют в базе: строка №") + QString::number(row));
            }
            else {
                for (int col = 1; col < this->columnCount(); col++)   {
                    QAxObject * cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", row, col);
                    record.setValue(col-1, cell->property("Value").toString());
                    delete cell;
                }
                cntNewRow++;
//                qDebug() << record;
                this->insertRecord(-1, record);
            }
        }
        // memory clearing
        delete StatSheet;
        delete sheets;
        workbook->dynamicCall("Close (Boolean)", false);
        delete workbook;
        delete workbooks;
        excel->dynamicCall("Quit(void)");
        delete excel;

//        this->setFilter(filter);

        QMessageBox::information(0,
                                 tr("Импорт данных"),
                                 tr("Строк добавлено: ") + QString::number(cntNewRow));
    }
}


void BartProxyModel::updateFromFile()   {
    QString fileName = QFileDialog::getOpenFileName(0,
                                                    tr("Обновление данных"),
                                                    QCoreApplication::applicationDirPath(),
                                                    tr ("Microsoft Excel (*.xls)"));
    if (!fileName.isEmpty())    {
        QString filter = this->filter();
        this->setFilter("");
        this->select();

        int cntUpdatedRow = 0;

        QAxObject * excel = new QAxObject("Excel.Application",this);
        QAxObject * workbooks = excel->querySubObject("Workbooks");
        QAxObject * workbook = workbooks->querySubObject("Open(const QString&)", fileName);
        QAxObject * sheets = workbook->querySubObject("Sheets");
        QAxObject * StatSheet = sheets->querySubObject("Item(1)");
        StatSheet->dynamicCall("Select()");

        QSqlRecord record = this->record();
        record.remove(0);

        for (int row = 1; !StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").isNull(); row++)   {
//            bool checkStrain = (StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 4)->property("Value").toString() == this->data(this->index(0,4)));
//            int checkName = this->findDatainRow(StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").toString());
            int checkRow = this->findDuplicate(StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 1)->property("Value").toString(),
                                               StatSheet->querySubObject("Cells(QVariant,QVariant)", row, 4)->property("Value").toInt());

            if (checkRow == -1)   {
                QMessageBox::information(0,
                                         tr("Обновление данных"),
                                         tr("Такие данные отсутствуют в базе данных: строка №") + QString::number(row));
            }
            else {
                for (int col = 1; col < this->columnCount(); col++)   {
                    QAxObject * cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", row, col);
                    record.setValue(col-1, cell->property("Value").toString());
                    delete cell;
                }
                cntUpdatedRow++;
            }
        }
        // memory clearing
        delete StatSheet;
        delete sheets;
        workbook->dynamicCall("Close (Boolean)", false);
        delete workbook;
        delete workbooks;
        excel->dynamicCall("Quit(void)");
        delete excel;

        //this->setFilter(filter);

        QMessageBox::information(0,
                                 tr("Обновление данных"),
                                 tr("Строк изменено: ") + QString::number(cntUpdatedRow));
    }
}


////////////////////////////////////////////////////////////////////////////////////////




//void BartProxyModel::insertRow()  {
//    int newRow = this->rowCount();
//    int newID = this->data(this->index(newRow-1, 0)).toInt() + 1;
//    QString parentID = this->data(this->index(0, 4)).toString();

////    this->insertRow(newRow);
//    this->setData(this->index(newRow, 0), newID);
//    this->setData(this->index(newRow, 4), parentID);
//}


//void BartProxyModel::insertRowData(QStringList &listData)
//{
//    int newRow = this->rowCount();

////    qDebug() << listData;
////    this->insertRow(newRow);
//    for (int i = 0; i < listData.count(); i++)  {
//        this->setData(this->index(newRow, i), listData.at(i));
//    }
//}


//void BartProxyModel::updateRowData(int row, QSqlRecord record)
//{
//    qDebug() << listData;
//    for (int i = 0; i < record.count(); i++)  {
//        this->setRecord(this->index(row, i), record.value(i).toString());
//    }
//}
