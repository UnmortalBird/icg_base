#-------------------------------------------------
#
# Project created by QtCreator 2019-12-14T14:12:57
#
#-------------------------------------------------

QT       += core gui sql printsupport axcontainer

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ICG_base
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    sqlconnectiondialog.cpp \
    strainsheetform.cpp \
    dbpathesdialog.cpp \
    valueslistdialog.cpp \
    iformwidget.cpp \
    dbfacade.cpp \
    tablefieldsdialog.cpp \
    bartproxymodel.cpp \
    secondarywindow.cpp \
    groupeditparamdialog.cpp

HEADERS += \
        mainwindow.h \
    sqlconnectiondialog.h \
    strainsheetform.h \
    dbpathesdialog.h \
    valueslistdialog.h \
    iformwidget.h \
    dbfacade.h \
    tablefieldsdialog.h \
    bartproxymodel.h \
    secondarywindow.h \
    groupeditparamdialog.h

FORMS += \
        mainwindow.ui \
    sqlconnectiondialog.ui \
    newstraindialog.ui \
    strainsheetform.ui \
    dbpathesdialog.ui \
    strainclassdialog.ui \
    valueslistdialog.ui \
    tablefieldsdialog.ui \
    secondarywindow.ui \
    groupeditparamdialog.ui

RESOURCES +=

RC_FILE     = resources.rc
