#include "dbpathesdialog.h"
#include "ui_dbpathesdialog.h"

DBPathesDialog::DBPathesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DBPathesDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Место хранения резервной копии");

    QSettings settings("settings.ini", QSettings::IniFormat);

    ui->editStrainPath->setText(settings.value("Path/BackupPath").toString());
//    ui->editColonyPath->setText(settings.value("Path/ColonyDB").toString());
//    ui->editBioChemPath->setText(settings.value("Path/BioChemDB").toString());
//    ui->editSeqPath->setText(settings.value("Path/SeqDB").toString());
//    ui->editAntiBioPath->setText(settings.value("Path/AntiBioDB").toString());
//    ui->editSubstratsPath->setText(settings.value("Path/SubstratsDB").toString());

    connect(ui->btnOK,              &QPushButton::clicked,      this,       &DBPathesDialog::okButtonClicked);
    connect(ui->btnCancel,          &QPushButton::clicked,      this,       &DBPathesDialog::reject);
    connect(ui->btnStrainPath,      &QPushButton::clicked,      this,       &DBPathesDialog::strainButtonClicked);
//    connect(ui->btnColonyPath,      &QPushButton::clicked,      this,       &DBPathesDialog::colonyButtonClicked);
//    connect(ui->btnBioChemPath,     &QPushButton::clicked,      this,       &DBPathesDialog::bioChemButtonClicked);
//    connect(ui->btnSeqPath,         &QPushButton::clicked,      this,       &DBPathesDialog::seqButtonClicked);
//    connect(ui->btnAntiBioPath,     &QPushButton::clicked,      this,       &DBPathesDialog::antiBioButtonClicked);
//    connect(ui->btnSubstratsPath,   &QPushButton::clicked,      this,       &DBPathesDialog::substratsButtonClicked);
}

DBPathesDialog::~DBPathesDialog()
{
    delete ui;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
QString DBPathesDialog::strainPath() const
{
    return ui->editStrainPath->text();
}


//QString DBPathesDialog::colonyPath() const
//{
//    return ui->editColonyPath->text();
//}


//QString DBPathesDialog::bioChemPath() const
//{
//    return ui->editBioChemPath->text();
//}


//QString DBPathesDialog::seqPath() const
//{
//    return ui->editBioChemPath->text();
//}


//QString DBPathesDialog::antiBioPath() const
//{
//    return ui->editAntiBioPath->text();
//}


//QString DBPathesDialog::substratsPath() const
//{
//    return ui->editSubstratsPath->text();
//}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DBPathesDialog::strainButtonClicked()
{
    QString dirName = QFileDialog::getExistingDirectory(this, tr("Резервная копия"), ui->editStrainPath->text());
    if (!dirName.isEmpty())
        ui->editStrainPath->setText(dirName);
}


//void DBPathesDialog::colonyButtonClicked()
//{
//    QString fileName = QFileDialog::getOpenFileName(this,
//                                                    tr("Выбрать файл базы данных по колониям"),
//                                                    "",
//                                                    "*.csv");
//    if (!fileName.isEmpty())
//        ui->editColonyPath->setText(fileName);
//}


//void DBPathesDialog::bioChemButtonClicked()
//{
//    QString fileName = QFileDialog::getOpenFileName(this,
//                                                    tr("Выбрать файл базы данных по биохимии"),
//                                                    "",
//                                                    "*.csv");
//    if (!fileName.isEmpty())
//        ui->editBioChemPath->setText(fileName);
//}


//void DBPathesDialog::seqButtonClicked()
//{
//    QString fileName = QFileDialog::getOpenFileName(this,
//                                                    tr("Выбрать файл базы данных по секвенированию"),
//                                                    "",
//                                                    "*.csv");
//    if (!fileName.isEmpty())
//        ui->editSeqPath->setText(fileName);
//}


//void DBPathesDialog::antiBioButtonClicked()
//{
//    QString fileName = QFileDialog::getOpenFileName(this,
//                                                    tr("Выбрать файл базы данных по антибиотикам"),
//                                                    "",
//                                                    "*.csv");
//    if (!fileName.isEmpty())
//        ui->editAntiBioPath->setText(fileName);
//}


//void DBPathesDialog::substratsButtonClicked()
//{
//    QString fileName = QFileDialog::getOpenFileName(this,
//                                                    tr("Выбрать файл базы данных по субстратам"),
//                                                    "",
//                                                    "*.csv");
//    if (!fileName.isEmpty())
//        ui->editSubstratsPath->setText(fileName);
//}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DBPathesDialog::okButtonClicked()
{
    if (ui->editStrainPath->text() == "")   {
        QMessageBox::information(this, tr("Файл базы данных штаммов не указан"),
                                 tr("Введите путь файла базы данных штаммов"));
        ui->editStrainPath->setFocus();
        return;
    }
//    if (ui->editColonyPath->text().isEmpty()) {
//        QMessageBox::information(this, tr("Файл базы данных групп не указан"),
//                                 tr("Введите путь файла базы данных групп"));
//        ui->editColonyPath->setFocus();
//        return;
//    }
//    if (ui->editBioChemPath->text().isEmpty()) {
//        QMessageBox::information(this, tr("Файл базы данных биохимии не указан"),
//                                 tr("Введите путь файла базы данных биохимии"));
//        ui->editBioChemPath->setFocus();
//        return;
//    }
//    if (ui->editSeqPath->text().isEmpty()) {
//        QMessageBox::information(this, tr("Файл базы данных секвенирования не указан"),
//                                 tr("Введите путь файла базы данных секвенирования"));
//        ui->editSeqPath->setFocus();
//        return;
//    }
//    if (ui->editAntiBioPath->text().isEmpty()) {
//        QMessageBox::information(this, tr("Файл базы данных антибиотиков не указан"),
//                                 tr("Введите путь файла базы данных антибиотиков"));
//        ui->editAntiBioPath->setFocus();
//        return;
//    }
//    if (ui->editSubstratsPath->text().isEmpty()) {
//        QMessageBox::information(this, tr("Файл базы данных субстратов не указан"),
//                                 tr("Введите путь файла базы данных субстратов"));
//        ui->editSubstratsPath->setFocus();
//        return;
//    }
    accept();
}
