#ifndef STRAINSHEETFORM_H
#define STRAINSHEETFORM_H

#include <QWidget>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QFontMetrics>
#include <QPlainTextEdit>
#include <QDateEdit>
#include <QSize>
#include <QScrollBar>
#include <QPushButton>
#include <QIntValidator>
#include <QRegExpValidator>
#include <QRegExp>
#include <QSignalMapper>
#include <QSettings>
#include <QCheckBox>
#include <QGroupBox>
#include <QSqlTableModel>
#include <QDebug>

#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QDesktopServices>

#include <QSqlRecord>

#include "iformwidget.h"
#include "bartproxymodel.h"

namespace Ui {
class StrainSheetForm;
}

class StrainSheetForm : public QWidget
{
    Q_OBJECT

public:
    explicit StrainSheetForm(QVector<QStringList> &vecValues, BartProxyModel *second_1, BartProxyModel *second_2, QWidget *parent = 0);
    ~StrainSheetForm();

    void createTable(QString tableName);
    void setID(QString tableName, int _id);

signals:
    void strainNewItem(StrainSheetForm * sheet, QString tableName);
    void strainDataAdded(QStringList &values, QString iwidgetID);
    void strainDataChanged(QStringList &values, QString tableID, QString strainID, QString iwidgetID);

private slots:

private:
    void editTable(IFormWidget *iwidget);
    void applyChanges(IFormWidget *iwidget, bool NewItem);
    void abortChanges(IFormWidget *iwidget, bool NewItem);

    void applyChanges(bool NewItem);
    void abortChanges();
    void editTable();
    void seqSetFile(QLineEdit *edit);
    void seqDirOpen(QString path);
    void seqFileOpen(QString path);

    void fillEmptyBox(QGroupBox *box, IFormWidget *iwidget);

    void setBaseStringList();
    void setColonyStringList();
    void setBioChemStringList();
    void setBoxData(QGroupBox *box, QStringList &values);
    void setBoxData(QGroupBox *box, IFormWidget *iwidget, QStringList &values);
    void setViewButtons(QGroupBox *box, bool NewItem);
    void setViewButtons(QGroupBox *box, IFormWidget *iwidget, bool NewItem);

    void setBoxDataDuo(QGroupBox *box, BartProxyModel *model);
    void setViewButtonsDuo(QGroupBox *box);
    void editTableDuo();
    void applyChangesDuo();
    void abortChangesDuo();
    void addRow();
    void delRow();
    void exportData();
    void importData();
    void updateData();

    Ui::StrainSheetForm *           ui;
    QString                         id;
    IFormWidget *                   wBase;
    IFormWidget *                   wColony;
    IFormWidget *                   wBioChem;
    BartProxyModel *                mAntiBio;
    BartProxyModel *                mSubstrats;
    QStringList                     listSeq;
    QString                         idSeq;
};

#endif // STRAINSHEETFORM_H
