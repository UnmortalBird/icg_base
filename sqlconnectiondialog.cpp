#include "sqlconnectiondialog.h"

SqlConnectionDialog::SqlConnectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SqlConnectionDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Настройки соединения с БД");

    QStringList drivers = QSqlDatabase::drivers();
    // remove compat names
    drivers.removeAll("QMYSQL3");
    drivers.removeAll("QOCI8");
    drivers.removeAll("QODBC3");
    drivers.removeAll("QPSQL7");
    drivers.removeAll("QTDS7");

    ui->comboDriver->addItems(drivers);

    QSettings settings("settings.ini", QSettings::IniFormat);

    ui->comboDriver->setCurrentText(settings.value("DatabaseConnection/Driver").toString());
    ui->editConnectionname->setText(settings.value("DatabaseConnection/ConnectionName").toString());
    ui->editDatabase->setText(settings.value("DatabaseConnection/DatabaseName").toString());
    ui->editUsername->setText(settings.value("DatabaseConnection/UserName").toString());
    ui->editHostname->setText(settings.value("DatabaseConnection/HostName").toString());
    ui->editPassword->setText(settings.value("DatabaseConnection/Password").toString());
    ui->portSpinBox->setValue(settings.value("DatabaseConnection/Port").toInt());

//   this->setModal(false);
}


SqlConnectionDialog::~SqlConnectionDialog()
{
    delete ui;
}

QString SqlConnectionDialog::driverName() const
{
    return ui->comboDriver->currentText();
}

QString SqlConnectionDialog::connectionName() const
{
    return ui->editConnectionname->text();
}

QString SqlConnectionDialog::databaseName() const
{
    return ui->editDatabase->text();
}

QString SqlConnectionDialog::userName() const
{
    return ui->editUsername->text();
}

QString SqlConnectionDialog::password() const
{
    return ui->editPassword->text();
}

QString SqlConnectionDialog::hostName() const
{
    return ui->editHostname->text();
}

int SqlConnectionDialog::port() const
{
    return ui->portSpinBox->value();
}


void SqlConnectionDialog::on_okButton_clicked()
{
    if (ui->comboDriver->currentText().isEmpty()) {
        QMessageBox::information(this, tr("No database driver selected"),
                                 tr("Please select a database driver"));
        ui->comboDriver->setFocus();
    } else {
        accept();
    }
}
