#ifndef VALUESLISTDIALOG_H
#define VALUESLISTDIALOG_H

#include <QDialog>
#include <QSettings>
#include <QStringList>
#include <QStringListModel>
#include <QTextCodec>
#include <QDebug>

namespace Ui {
class ValuesListDialog;
}


class ValuesListDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ValuesListDialog(QString groupName, QString itemName, QString dialogName = "Настройка списка параметров", QWidget *parent = 0);
    ~ValuesListDialog();

    QStringList value_list();

private slots:
    void btnOK_clicked();
    void btnAbort_clicked();
    void btnAdd_clicked();
    void btnEdit_clicked();
    void btnDel_clicked();

private:
    Ui::ValuesListDialog *      ui;
    QStringListModel *          model;
};


#endif // VALUESLISTDIALOG_H
