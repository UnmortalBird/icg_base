#include "secondarywindow.h"
#include "ui_secondarywindow.h"


SecondaryWindow::SecondaryWindow(QString windowName, BartProxyModel *secModel, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SecondaryWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(windowName);

    initMenu();
    ui->cBoxSrch->addItem("ID штамма");
    ui->cBoxSrch->addItem("Название");
    ui->cBoxSrch->addItem("Комментарий");

//    model->setFilter("idAntiBio < 10");
    model = secModel;
    ui->tableView->setModel(model);
    setTableView();

    connect(ui->lineSrch,                       &QLineEdit::returnPressed,          this,       &SecondaryWindow::search);
    connect(ui->btnSrch,                        &QPushButton::clicked,              this,       &SecondaryWindow::search);
//    connect(ui->btnSumTable,                    &QPushButton::clicked,              this,       &SecondaryWindow::sumTable);
//    connect(ui->tableView->verticalHeader(),    &QHeaderView::sectionClicked,       this,       &SecondaryWindow::selectHeader);
}


SecondaryWindow::~SecondaryWindow()
{
    delete ui;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SecondaryWindow::initMenu()    {
    QMenu *fileMenu = menuBar()->addMenu("Данные");

    fileMenu->addAction(QObject::tr("Добавить запись"),
                      [&]()     {   addRecord();    }   );
    fileMenu->addAction(QObject::tr("Удалить запись"),
                      [&]()     {   delRecord();    }   );
    fileMenu->addSeparator();

    fileMenu->addAction(QObject::tr("Экспорт в файл"),
                      [&]()     {   model->exportToFile();   }   );
    fileMenu->addAction(QObject::tr("Импорт из файла"),
                      [&]()     {   model->importFromFile();   }   );
    fileMenu->addAction(QObject::tr("Обновить из файла"),
                      [&]()     {   model->updateFromFile();   }   );
    fileMenu->addSeparator();

    fileMenu->addAction(QObject::tr("Сохранить изменения"),
                      [&]()     {   applyChanges(); }   );
    fileMenu->addAction(QObject::tr("Отменить изменения"),
                      [&]()     {   abortChanges(); }   );
}


void SecondaryWindow::setTableView()    {
    BartProxyModel *model = static_cast<BartProxyModel *>(ui->tableView->model());
    if(model->database().isOpen())  {
        model->setEditStrategy(QSqlTableModel::OnManualSubmit);
        ui->tableView->model()->setHeaderData(0, Qt::Horizontal, "id");
        ui->tableView->model()->setHeaderData(1, Qt::Horizontal, "Название");
        ui->tableView->model()->setHeaderData(2, Qt::Horizontal, "Реакция");
        ui->tableView->model()->setHeaderData(3, Qt::Horizontal, "Комментарий");
        ui->tableView->model()->setHeaderData(4, Qt::Horizontal, "ID штамма");
        ui->tableView->setSortingEnabled(true);
        ui->tableView->resizeColumnsToContents();
        ui->tableView->horizontalHeader()->setSectionHidden(0, true);
        ui->tableView->horizontalHeader()->setSortIndicator(1, Qt::AscendingOrder);
        ui->tableView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
        //ui->tableView->setSelectionMode(QAbstractItemView::MultiSelection);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SecondaryWindow::search()   {
    if (ui->lineSrch->text().isEmpty()) return;

    QString param = "";
    QString value = ui->lineSrch->text();
    BartProxyModel *model = static_cast<BartProxyModel *>(ui->tableView->model());

    int idx = 0;
    while (model->headerData(idx, Qt::Horizontal) != ui->cBoxSrch->currentText())   {   idx++;  }
    param = model->record().fieldName(idx);

    if (value != "*")
        model->setFilter(QString("%1 LIKE '%2%'").arg(param, value));
    else
        model->setFilter(QString("%1 > 0").arg(model->record().fieldName(0)));
    setTableView();
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SecondaryWindow::addRecord()    {
    BartProxyModel * model = static_cast<BartProxyModel *>(ui->tableView->model());

    QSqlRecord record = model->record();
    record.remove(0);
    model->insertRecord(-1, record);

    ui->tableView->selectRow(model->rowCount() - 1);
    ui->tableView->setFocus();
}


void SecondaryWindow::delRecord()    {
    QModelIndexList list = ui->tableView->selectionModel()->selectedRows();

    for (int i = 0; i < list.count(); i++)
        ui->tableView->model()->removeRow(list[i].row());
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SecondaryWindow::applyChanges()   {
    static_cast<BartProxyModel *>(ui->tableView->model())->submitAll();
}


void SecondaryWindow::abortChanges()   {
    static_cast<BartProxyModel *>(ui->tableView->model())->revertAll();
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////

//void SecondaryWindow::selectHeader(int logicalIndex)    {
//    BartProxyModel *model = static_cast<BartProxyModel *>(ui->tableView->model());
//    qDebug() <<model->index(logicalIndex,0);
//    qDebug() << ui->tableView->verticalHeader()->selectionModel()->selectedRows().contains(model->index(logicalIndex,0));

////qDebug() << logicalIndex;

////    ui->tableView->selectionModel()->select(model->index(logicalIndex, 1), QItemSelectionModel::Toggle);

//    QModelIndexList list = ui->tableView->selectionModel()->selectedRows();
//    qDebug() << list;
//}

//void SecondaryWindow::sumTable()    {
//    BartProxyModel *model = static_cast<BartProxyModel *>(ui->tableView->model());

//    QModelIndexList list = ui->tableView->selectionModel()->selectedRows();
//    qDebug() << list;
//    for (int i = 0; i < list.count(); i++) {
//        qDebug() << model->data(model->index(list[i].row(), 1)).toString();
//    }
//}
